import os
import scipy as sp
from scipy import linalg as la
from scipy.stats import norm

from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process import GaussianProcess
from sklearn.gaussian_process.kernels import RBF
from sklearn.gaussian_process.kernels import WhiteKernel
# from sklearn.gaussian_process.kernels import Matern
# from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
# from sklearn.neural_network import MLPClassifier
# from sklearn.ensemble import AdaBoostClassifier
# from sklearn.ensemble import RandomForestClassifier

from sklearn.externals import joblib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def GP_prob(extra_points, X_points, y):
    # ARTIFICIAL POINTS
    x = sp.linspace(min(X_points[:, 0]), max(X_points[:, 0]), 10)
    # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones((x.shape[0], 1))
    z_m = sp.zeros((x.shape[0], 1))
    print(y.shape, z_p.shape)
    X_points = sp.vstack((X_points,
                          sp.vstack((x, y_p)).T,
                          sp.vstack((x, y_m)).T))
    y = sp.vstack((y, z_p, z_m))

    """
    clf = RandomForestClassifier(n_estimators=100)
    std_scaler = StandardScaler()
    # std_scaler.fit(X_points)
    # X_points = std_scaler.transform(X_points)
    # extra_points = std_scaler.transform(extra_points)

    clf.fit(X_points, y.ravel())
    print(clf.get_params())
    y_pred = clf.predict_proba(extra_points)
    y_pred = y_pred[:, 1]
    gp = clf
    """

    # Using probit transformation to produce bounded probabilities.
    """
    len_scale = sp.ones(X_points.shape[-1]) * 5e0
    len_scale[0] = 1e-2
    len_scale_bounds = sp.array([[5e-1, 1e5], [1e-2, 1e2]])
    print(len_scale)
    kern_rbf = RBF(length_scale=len_scale, length_scale_bounds=len_scale_bounds)
    # kern_mat = Matern(len_scale, len_scale_bounds, nu=2.5)
    gp = GaussianProcessRegressor(kernel=kern_rbf,
                                  alpha=1e-1,
                                  normalize_y=False,
                                  n_restarts_optimizer=2)
    # gp = GaussianProcess(nugget=1e-3)

    # since correlation function is the default squared exponential
    # auto-correlation function, nugget is variance of the input values.

    # Transforming into [-inf, inf] domain, inverse probit, \Phi^{-1}(y)
    y[y == 1] = 1 - 1e-2
    y[y == 0] = 1e-2

    P_y = norm.ppf(y)

    # P_y[sp.isposinf(P_y)] = sp.amax(P_y[sp.isfinite(P_y)])
    # P_y[sp.isneginf(P_y)] = sp.amin(P_y[sp.isfinite(P_y)])

    # Gaussian process regression using \Phi^{-1}(y) as target.
    std_scaler = None
    gp.fit(X_points, P_y)
    print("GP params:")
    print(gp.get_params())
    # print(gp.kernel.get_params())
    # print(gp.kernel.theta)

    y_pred = gp.predict(extra_points)

    # Transforming back into [0, 1] domain, probit, \Phi(y) and norm
    y_pred = norm.cdf(y_pred)
    # Normalising
    # y_pred = y_pred / (sp.sum(y_pred, axis=1)[:, None])
    """

    """
    # Simple GP regression
    gp = GaussianProcess(nugget=10**-2)
    std_scaler = None
    gp.fit(X_points, y)
    y_pred = gp.predict(extra_points)
    # """

    # """
    # Gaussian process classifier
    len_scale = sp.ones(X_points.shape[-1]) * 5e0
    len_scale[1] = 1e-2
    len_scale_bounds = sp.array([[5e-1, 1e5], [1e-3, 1e2]])
    kern_rbf = RBF(length_scale=len_scale,
                   length_scale_bounds=len_scale_bounds)  # +\
    # WhiteKernel(noise_level=1e-3)
    gp = GaussianProcessClassifier(kernel=kern_rbf,
                                   n_restarts_optimizer=2,
                                   max_iter_predict=200,
                                   warm_start=True,
                                   n_jobs=-2)
    std_scaler = None

    # gp = KNeighborsClassifier(n_neighbors=3, n_jobs=-2)
    # std_scaler = StandardScaler()
    # std_scaler.fit(X_points)
    # X_points = std_scaler.transform(X_points)
    # extra_points = std_scaler.transform(extra_points)

    # gp = MLPClassifier(activation='logistic',
    # alpha=0,
    # max_iter=int(1e5),
    # tol=1e-10)
    # std_scaler = None

    # std_scaler = StandardScaler()
    # std_scaler.fit(X_points)
    # X_points = std_scaler.transform(X_points)
    # extra_points = std_scaler.transform(extra_points)

    print(X_points.shape, y.shape)
    gp.fit(X_points, sp.ravel(y))
    print("GP params:")
    print(gp.get_params())
    print(gp.classes_)

    y_pred = gp.predict_proba(extra_points)
    y_pred = y_pred[:, 1]
    # """
    return y_pred, gp, std_scaler


def plot_GP(xx, yy, y_plot, X, Y):
    fig = plt.figure()
    ax = fig.gca(projection='3d')               # 3d axes instance
    surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arryas)
                             # rstride=2,           # row step size
                             # cstride=2,           # column step size
                             # cmap=cm.RdPu,        # colour map
                             # linewidth=1,         # wireframe line width
                             antialiased=True)
    ax.scatter(X[:, 0], X[:, 1], Y,
               marker='x',
               c='r')
    ax.set_xlabel('Methylation ($m$)')
    ax.set_ylabel('Ligand concentration ($L$)')
    ax.set_zlabel('Probability satisfaction for $\phi_{RUN}$')

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones(x.shape)
    z_m = sp.zeros(x.shape)
    ax.plot(x, y_p, z_p, color='r')
    ax.plot(x, y_m, z_m, color='r')
    plt.show()


def train_GP(run=True, plot=False):
    if run == 'both':
        m_L_prphi = sp.load('m_L_prphi_True.npy')
        m_L_prphi = sp.vstack((m_L_prphi, sp.load('m_L_prphi_False.npy')))
    else:
        m_L_prphi = sp.load('m_L_prphi_' + str(run) + '.npy')

    sample_size = min(2000, len(m_L_prphi))

    # m_L_prphi_Run = m_L_prphi[m_L_prphi[:, -1] >= 0.5]
    # m_L_prphi_Tumble = m_L_prphi[m_L_prphi[:, -1] < 0.5]
    # sample_size = min(5000, len(m_L_prphi_Run), len(m_L_prphi_Tumble))

    print("sample size:", sample_size)
    # Building sample without losing edge points
    # sample_idx_run = sp.random.choice(len(m_L_prphi_Run), len(
    #     m_L_prphi_Run) - sample_size, replace=False)
    # sample_idx_tumble = sp.random.choice(len(m_L_prphi_Tumble), len(
    #     m_L_prphi_Tumble) - sample_size, replace=False)

    # m_L_prphi = sp.vstack((
    #     sp.delete(m_L_prphi_Run, sample_idx_run, axis=0),
    #     sp.delete(m_L_prphi_Tumble, sample_idx_tumble, axis=0)))

    sample_idx = sp.random.choice(len(m_L_prphi),
                                  sample_size, replace=False)

    m_L_prphi = m_L_prphi[sample_idx]

    X = m_L_prphi[:, :2]
    Y = m_L_prphi[:, -1]
    if sp.any(Y < 0):
        print("Y<0")
        Y[Y < 0] = 0
    Y[Y > 1] = 1

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    # points in the y axis
    y = sp.linspace(min(X[:, 1]), max(X[:, 1]), 100)
    xx, yy = sp.meshgrid(x, y)                # create the "base grid"
    x_pred = sp.column_stack((xx.ravel(), yy.ravel()))
    print(x_pred.shape)

    # GP for estimating phi_{RUN}
    y_gp = Y.reshape((len(Y), 1))
    # y_gp = sp.hstack((y_gp, sp.ones(sp.shape(y_gp)) - y_gp))
    y_pred, gp, std_scaler = GP_prob(x_pred, X, y_gp)
    # y_pred = y_pred[:, 0]
    y_plot = y_pred.reshape(sp.shape(xx))

    # Saving trained GP
    joblib.dump(gp, 'gp_phi_run_' + str(run) + '.pkl')
    if std_scaler is not None:
        print("Scaler on.")
        joblib.dump(std_scaler, 'std_scaler_' + str(run) + '.pkl')

    if plot:
        plot_GP(xx, yy, y_plot, X, Y)


if __name__ == '__main__':
    train_GP(run=True, plot=True)
    train_GP(run=False, plot=True)
    # train_GP(run='both', plot=True)
