# Stochastic Simulation Algorithm input file
# Flag-motor in E. coli, no delay
# CW_S (--> CW_C) --> CCW_N --> CW_S

# Reactions
Flag1:
    S > C
    S*mu

Motor1a:
    C > N
    C*k_plus
  	# *mu/(mu-k_plus)

Motor1b:
    S > N
    S*k_plus

Motor2:
    N > S
    N*k_minus

# Fixed species
 
# Variable species
S = 10.0
C = 0.0
N = 0.0
 
# Parameters
mu = 5.0
k_plus = 0.1
k_minus = 0.2
