 
addpath('GPC');


runs = 100;

X_struct = load('m_L_prphi.mat');
X_prphi = X_struct.x;
X_prphi = X_prphi(randperm(size(X_prphi, 1)), :);
X_prphi = X_prphi(1:runs, :);
X = X_prphi(:, 1:2);
y = X_prphi(:, end);

% Prediction points
xt = linspace(min(X(:, 1)), max(X(:, 1)), 100);
yt = linspace(min(X(:, 2)), max(X(:, 2)), 100);
[xx, yy] = meshgrid(xt, yt);
Xt = [reshape(xx, [], 1), reshape(yy, [], 1)];

% amplitude = 1.616202248415312;
% lengthscale = 1.262790389943146;
amplitude = 1;
lengthscale = [10 10];


% FUN FACT: The log-likelihood will break for these values 
%           given 33 nodes for the quadrature
%           i.e. ridiculous values + really unstable wrt the hyperparams
% amplitude = 2.71; lengthscale = 0.81;
% amplitude = 2.7; lengthscale = 0.81;
%
% SOLUTION: increase the nodes for the quadrature




% BOTOND INITIALISATION
CovFunc=@CovSE;
covpar = log([amplitude^2, 1./lengthscale.^2]);
options.eps_damp=0.5;
options.covCORR=1e-4;
Grad_ON=1;

likPar.p = y * runs;
likPar.q = (1 - y) * runs;

likPar.p = ceil(likPar.p);
likPar.q = ceil(likPar.q);

%covpar = GPEP_opt(CovFunc, covpar, X, @logprobitpow, likPar, options.eps_damp, options.covCORR, Grad_ON);
amplitude = sqrt(exp(covpar(1)))
lengthscale = 1 ./ sqrt(exp(covpar(2:end)))


%  BOTOND LOG-LIKELIHOOD
botond_loglik = - GPEP(CovFunc, covpar, X, @logprobitpow, likPar, options.eps_damp, options.covCORR, Grad_ON);



%  BOTOND GP
tic;
[logZ, dlogZ,Gauss, Diag] =  GPEP(CovFunc, covpar, X, @logprobitpow, likPar, options.eps_damp, options.covCORR, Grad_ON);
toc


ks = feval(CovFunc,covpar,Xt,X,0)';
kss = diag(feval(CovFunc,covpar,Xt,Xt,0));

v_tilde = Gauss.Term(:,1);
tau_tilde = Gauss.Term(:,2);
diagSigma_tilde = 1./tau_tilde;
mu_tilde = diagSigma_tilde .* v_tilde;

temp = ks' * inv(Gauss.C + diag(diagSigma_tilde));
fs = temp * (mu_tilde);
vfs = kss - diag(temp * ks);
botond_pi = normcdf(fs./sqrt(1+vfs));





botond_pi = normcdf(fs./sqrt(1+vfs));
botond_lb = normcdf((fs - 2 * sqrt(vfs)) ./ sqrt(1 + vfs));
botond_ub = normcdf((fs + 2 * sqrt(vfs)) ./ sqrt(1 + vfs));


results = [Xt, botond_pi, botond_lb, botond_ub];
% results(max(1, end-40):end, :);

fprintf('log-likelihood: %.12f\n', botond_loglik);

figure;
% plot(X, y, 'o', Xt, botond_pi, 'r', Xt, botond_lb, 'k', Xt, botond_ub, 'k')
botond_pi = reshape(botond_pi, size(xx));
mesh(xx, yy, botond_pi)
hold on
scatter3(X(:, 1), X(:, 2), y)
%title('Posterior f')

%  figure;
%  plot(X, y, 'o', Xt, fs, 'r', Xt, fs - 2 * sqrt(vfs), 'k', Xt, fs + 2 * sqrt(vfs), 'k')
%  title('Posterior g')
