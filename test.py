import scipy as sp
import os
from scipy import stats
from config import *
import GPy
from scipy.stats import norm
import pickle
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Loading trained GP for phi_RUN
N_trials = 5
# Pickle + GPy
smodel = 'gpEP_phi_run.pkl'
gpEP = pickle.load(open(smodel, 'rb'))
gp_param = gpEP['gpEP_param']
gp_posterior = gpEP['gpEP_posterior']
X = gpEP['gpEP_X']
Y = gpEP['gpEP_y'][:, 0]
y_trials = gpEP['gpEP_y_meta']
k_param = gpEP['gpEP_k_param']


def GP_prob(extra_points, X_points, y, y_meta=None, y_pred_meta=None):
    bi_lklhd = GPy.likelihoods.Binomial()
    # lengthscale = sp.ones(X_points.shape[1]) * \
    #     sp.array([max(X[:, 0]), max(X[:, 1])]) * 0.2
    kernel = GPy.kern.RBF(X_points.shape[1], ARD=True,
                          initialize=False)
    kernel.update_model(False)
    kernel.initialize_parameter()
    kernel[:] = k_param
    # kernel.update_model(True)
    # Model creation, without initialization:
    gp = GPy.core.GP(X_points, y,
                     kernel=kernel,
                     likelihood=bi_lklhd,
                     Y_metadata=y_meta,
                     initialize=False)

    # do not call the underlying expensive algebra on load
    gp.update_model(False)
    # Initialize the parameters (connect the parameters up)
    gp.initialize_parameter()
    gp[:] = gp_param  # Load the parameters
    gp.posterior = gp_posterior
    # gp.update_model(True)  # Call the algebra only once
    print(gp[:])
    print("hi")
    fs, vfs = gp._raw_predict(extra_points)
    y_pred_mean = norm.cdf(fs / sp.sqrt(1 + vfs))
    y_pred_sigma_lb = norm.cdf((fs - 2 * sp.sqrt(vfs)) / sp.sqrt(1 + vfs))
    y_pred_sigma_ub = norm.cdf((fs + 2 * sp.sqrt(vfs)) / sp.sqrt(1 + vfs))
    y_pred_sigma = (y_pred_sigma_lb, y_pred_sigma_ub)
    return y_pred_mean, y_pred_sigma, gp


x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
# points in the y axis
y = sp.linspace(min(X[:, 1]), max(X[:, 1]), 100)
xx, yy = sp.meshgrid(x, y)                # create the "base grid"
x_pred = sp.column_stack((xx.ravel(), yy.ravel()))

# GP for estimating phi_{RUN}
y_gp = Y.reshape((len(Y), 1))
# y_gp = sp.hstack((y_gp, sp.ones(sp.shape(y_gp)) - y_gp))

y_pred_trials = {'trials': (sp.ones((len(x_pred), 1)) * N_trials).astype(int)}


print(y_gp[:10])
print(y_gp.shape, y_trials['trials'].shape, y_pred_trials['trials'].shape)

y_pred, y_pred_sigma, gp = GP_prob(x_pred, X, y_gp,
                                   y_meta=y_trials, y_pred_meta=y_pred_trials)
# y_pred = y_pred[:, 0]
y_plot = y_pred.reshape(sp.shape(xx))
y_plot_sigma = (y_pred_sigma[0].reshape(sp.shape(xx)),
                y_pred_sigma[1].reshape(sp.shape(xx)))

# Plotting scatter of observations, wireframe of GP mean
fig = plt.figure()
ax = fig.gca(projection='3d')               # 3d axes instance
surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arryas)
                         # rstride=2,           # row step size
                         # cstride=2,           # column step size
                         # cmap=cm.RdPu,        # colour map
                         # linewidth=1,         # wireframe line width
                         color='blue',
                         antialiased=True)

surf_sigma_plus = ax.plot_wireframe(xx, yy, y_plot_sigma[0],
                                    antialiased=True, color='green')

surf_sigma_minus = ax.plot_wireframe(xx, yy, y_plot_sigma[1],
                                     antialiased=True, color='green')

ax.scatter(X[:, 0], X[:, 1], Y / y_trials['trials'][:, 0],
           marker='x',
           c='r')
ax.set_xlabel('Methylation ($m$)')
ax.set_ylabel('Ligand concentration ($L$)')
ax.set_zlabel('Probability satisfaction for $\phi_{RUN}$')
plt.show()
