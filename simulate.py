import scipy as sp
from scipy.linalg import norm
import Ecoli_nometh as Ecoli
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import os
import config
import io
from contextlib import redirect_stdout


def L(x):
    L = config.L_field(x)
    return L


def ecoli_sim(n=1, plot=True):
    # INITIAL CONDITIONS for E. coli
    t = int(0 * 100)
    t_end = int(config.t_end * 100)
    dt = int(config.dt * 100)
    pos = config.pos
    vel = config.vel
    state = config.state

    L_x = L(pos)
    # Calculating error of original position
    err = sp.linalg.norm(pos)
    print("Starting average error", err)
    err = 0
    ave_traj = sp.zeros((int(t_end / dt) + 1, 7))
    ave_traj[0] = sp.array(sp.hstack((t / 100, config.pos, config.pos ** 2,
                                      norm(config.pos), norm(config.pos) ** 2)))

    # Writing CTMC output to file
    out_dir = os.path.dirname(os.path.realpath(__file__))
    out_path = os.path.join(out_dir, config.outname)

    outfile = None
    if outfile is None:
        # with open(out_path, 'wb') as outfile:
        # Loop iterations = number of E. coli simulations
        for i in range(n):
            print("Ecoli", i + 1, "of", n)
            t = int(0 * 100)
            ecoli = Ecoli.Ecoli(state=state, parameters=None, L=L_x,
                                pos=pos, vel=vel, outfile=outfile)

            # print("L at", ecoli.pos, "=", L(ecoli.pos),
            #       "(E. coli position)")
            # print("L at", sp.zeros(2), "=", L(sp.zeros(2)))
            # print("E. coli Y_p: ", ecoli.Y_p)

            while t < t_end:
                # print(L(ecoli.pos))
                # print(L(ecoli.pos), ecoli.Y_p)
                # Squelching output
                with io.StringIO() as buf, redirect_stdout(buf):
                    ecoli.simulate(dt=config.dt, L=L(ecoli.pos))

                t += dt
                if t % dt == 0:
                    # print("Time elapsed:", t, "seconds.")
                    ave_traj[int(t / dt), 0] = t / 100
                    ave_traj[int(t / dt), 1:3] += ecoli.pos / n
                    ave_traj[int(t / dt), 3:5] += (ecoli.pos ** 2) / n
                    ave_traj[int(t / dt), 5] += norm(ecoli.pos) / n
                    ave_traj[int(t / dt), 6] += norm(ecoli.pos) ** 2 / n

            traj = sp.hstack((ecoli.t, ecoli.pos))
            ecoli.trajectory = sp.vstack((ecoli.trajectory, traj))

            # Dumping full E. coli trajectory and run/tumble times
            sp.save('traj' + str(i + 1), ecoli.trajectory)
            sp.save('run' + str(i + 1), ecoli.t_run)
            sp.save('tumble' + str(i + 1), ecoli.t_tumble)

            # Plotting
            t_run = []
            t_tumble = []
            if plot:
                # plot_ecoli(ecoli)

                # RUN/TUMBLE times histogram
                t_run += ecoli.t_run
                t_tumble += ecoli.t_tumble

            # Adding final E. coli position to error count
            err += sp.linalg.norm(ecoli.pos)

    # Calculating variance for ave_traj: Var[X] = E[X^2] - (E[X])^2
    ave_traj[:, 3:5] = ave_traj[:, 3:5] - ave_traj[:, 1:3] ** 2
    ave_traj[:, 6] = ave_traj[:, 6] - ave_traj[:, 5] ** 2

    if plot:
        # RUN/TUMBLE times histogram
        plt.figure()
        plt.hist(sp.array(t_run))
        plt.title("Run times histogram")
        plt.savefig("OUmeth_" + str(n) + "ecoli_hist_run.png")
        plt.figure()
        plt.hist(sp.array(t_tumble))
        plt.title("Tumble times histogram")
        plt.savefig("OUmeth_" + str(n) + "ecoli_hist_tumble.png")

    return err / n, ave_traj


def plot_ecoli(ecoli):
    # Plotting
    x_min = sp.hstack((ecoli.trajectory[:, 1],
                       ecoli.trajectory[:, -1])).min() - 1
    x_max = sp.hstack((ecoli.trajectory[:, 1],
                       ecoli.trajectory[:, -1])).max() + 1

    nx = 100  # int((x_max - x_min)/0.1)
    ny = nx
    y_min = x_min
    y_max = x_max

    x = sp.linspace(x_min, x_max, nx)
    y = sp.linspace(y_min, y_max, ny)

    xx, yy = sp.meshgrid(x, y)
    h = L(sp.vstack((xx.flatten(), yy.flatten())).T).reshape(nx, ny)

    fig = plt.figure()
    s = fig.add_subplot(1, 1, 1, xlabel='$x$', ylabel='$y$')
    im = s.imshow(
        h,
        extent=(x[0],
                x[-1],
                y[0],
                y[-1]),
        norm=LogNorm(vmin=h.min(), vmax=h.max()),  # L(sp.zeros(2)),
        origin='lower')
    fig.colorbar(im)

    cs = plt.cm.gray(ecoli.trajectory[:, 0])[0, 0]
    s.plot(ecoli.trajectory[0, 1], ecoli.trajectory[0, -1],
           c='black', marker='*', ms=10)
    s.plot(ecoli.trajectory[:, 1], ecoli.trajectory[:, -1],
           color=cs, marker='^', ms=3)

    # print(ecoli.trajectory)
    print(ecoli.state)
    print(ecoli.parameters)
    print("vel", ecoli.vel)
    print()
    print("Running statistics (mean, std dev):", len(ecoli.t_run))
    print(sp.mean(ecoli.t_run), sp.std(ecoli.t_run))
    print("Tumbling statistics (mean, std dev):", len(ecoli.t_tumble))
    print(sp.mean(ecoli.t_tumble), sp.std(ecoli.t_tumble))


err_tot, ave_traj = ecoli_sim(int(config.n_ecoli), plot=False)
print(err_tot)
print(ave_traj)
sp.save('ave_ecoli', ave_traj)
plt.show()
