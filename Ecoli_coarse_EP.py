import scipy as sp
from scipy import stats
from scipy.stats import norm

from config import *

import pickle


class Tumbling_angle_dist(stats.rv_continuous):

    def _pdf(self, x):
        return (0.5 * (1 + sp.cos(x)) * sp.sin(x))


class Ecoli:
    # USER SETTINGS
    # (MODEL TO BE USED, PARAMETERS)

    # Loading the model
    # Pickle
    smodel = 'gpEP_phi_run'

    tumbling_angle_dist = Tumbling_angle_dist()

    # Weiner variable for methylation OU process
    def dW(dt):
        return sp.random.normal(loc=0.0, scale=sp.sqrt(dt))

    def __init__(s, L=0.0,
                 pos=sp.ones(2) * 5, vel=sp.array([0.5, 0])):
        s.gp_True = s.load_GP(pkl_name=(Ecoli.smodel + '_FITC_True.pkl'))
        s.gp_False = s.load_GP(pkl_name=(Ecoli.smodel + '_FITC_False.pkl'))

        # Methylation
        # s.smod_meth = stochpy.SSA(IsInteractive=False, IsQuiet=True)
        # s.smod_meth.Model(Ecoli.smodel_meth)

        s.m = s.m_mean(L)
        s.L = L  # Doesn't really matter

        # Setting time, location, and velocity
        s.t = 0.0
        # keeping track of running, tumbling times
        s.t_run = []
        s.t_tumble = [0.0]
        s.run_flag = 0
        s.pos = sp.array(pos)
        s.vel = sp.array(vel)
        s.tumble()
        s.trajectory = sp.asmatrix(sp.hstack((0.0, s.pos)))

    def simulate(s, L=None, n_ecoli=1, dt=0.05):
        s.L = L

        extra_points = sp.array([[s.m, s.L]])

        if s.run_flag:
            gp = s.gp_True
        else:
            gp = s.gp_False

        ########################################
        # With sampling, p =  \Phi(N(fm, fs2)) #
        ########################################
        # fm, fs2 = gp.predict(extra_points)[2: 4]
        # fm = fm[:, 0]

        # y = sp.random.multivariate_normal(fm, fs2, 1)
        # p = gp.likfunc.cumGauss(None, y)
        # p_pred_mean = p[0, 0]

        ##################
        # Using the mean #
        ##################
        fm = gp.predict(extra_points)[0]
        p_pred_mean = fm / 2 + 0.5

        s.move(dt=dt, p_run=p_pred_mean)

        # Evolve m
        s.m += s.dm(L=L, dt=dt)
        s.m = max(0, s.m)

    def move(s, dt=0.05, p_run=None):
        s.t += dt

        if sp.random.uniform() > p_run:
            s.tumble(dt=dt)
            traj = sp.hstack((s.t, s.pos))
            s.trajectory = sp.vstack((s.trajectory, traj))
            # print(s.t)
            # Collecting tumbling times
            if s.run_flag:
                s.t_tumble += [dt]
                s.run_flag = 0
            else:
                s.t_tumble[-1] += dt
        else:
            s.run(dt=dt)
            # Collecting run times
            if s.run_flag:
                s.t_run[-1] += dt
            else:
                s.t_run += [dt]
                s.run_flag = 1

    def run(s, dt=0.05):
        s.pos += s.vel * dt

    def tumble(s, dt=0.05):
        # Assuming theta_dist gives absolute values
        sign = sp.random.choice(sp.array([-1, 1]))
        theta = theta_dist()
        theta = sign * theta

        cos_t = sp.cos(theta)
        sin_t = sp.sin(theta)
        R = sp.array([[cos_t, -sin_t], [sin_t, cos_t]])
        s.vel = R @ s.vel

    # HELPER FUNCTIONS
    def m_mean(s, L):
        g = g_0
        Y_ss = K_D * ((1 - 2 / g * sp.log(1 / mb_CW - 1)) /
                      (1 + 2 / g * sp.log(1 / mb_CW - 1))
                      )

        # Using Emonet's reply.
        m = -1 / eps_1 * (eps_0 +
                          n_TAR * (sp.log(1 + L / K_TAR_off) -
                                   sp.log(1 + L / K_TAR_on)) +
                          n_TSR * (sp.log(1 + L / K_TSR_off) -
                                   sp.log(1 + L / K_TSR_on)) -
                          sp.log(alpha / Y_ss - 1)
                          )
        # print(-1 / eps_1 * (eps_0 +
        #                     n_TAR * (sp.log(1 + L / K_TAR_off) -
        #                              sp.log(1 + L / K_TAR_on)) +
        #                     n_TSR * (sp.log(1 + L / K_TSR_off) -
        #                              sp.log(1 + L / K_TSR_on))))
        # print(1 / eps_1 * sp.log(alpha / Y_ss - 1))
        # print(alpha, Y_ss, alpha/Y_ss -1)
        if m < 0:
            print("Negative methylation.")
        m = max(0, m)
        return m

    def dm(s, L, dt=0.05):
        # Wiener process for methylation updates
        sigma_m = sp.sqrt(tau * C) / alpha
        dm = dt / tau * (s.m_mean(L) - s.m) \
            + sigma_m * sp.sqrt(2 / tau) * Ecoli.dW(dt)
        return dm

    def load_GP(s, pkl_name=smodel):
        # Loading for pyGPs
        gp = pickle.load(open(pkl_name, 'rb'))
        X_points = gp.x

        # ++++++++++++++++++++++++++++++++++
        # Making sure the correct EP regression function loaded
        # ++++++++++++++++++++++++++++++++++
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        x = sp.linspace(min(X_points[:, 0]), max(X_points[:, 0]),
                        100)  # points in the x axis
        # points in the y axis
        y = sp.linspace(min(X_points[:, 1]), max(X_points[:, 1]), 100)
        xx, yy = sp.meshgrid(x, y)                # create the "base grid"
        x_pred = sp.column_stack((xx.ravel(), yy.ravel()))
        y_pred = gp.predict(x_pred)[0] / 2 + 0.5
        y_sigma = gp.predict(x_pred)[1] / 4

        y_plot = y_pred.reshape(sp.shape(xx))
        y_sigma = y_sigma.reshape(sp.shape(xx))

        fig = plt.figure()
        ax = fig.gca(projection='3d')               # 3d axes instance
        surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arryas)
                                 # rstride=2,           # row step size
                                 # cstride=2,           # column step size
                                 # cmap=cm.RdPu,        # colour map
                                 # linewidth=1,         # wireframe line width
                                 color='blue')

        surf = ax.plot_wireframe(xx, yy, y_plot + y_sigma,      # data values (2D Arryas)
                         # rstride=2,           # row step size
                         # cstride=2,           # column step size
                         # cmap=cm.RdPu,        # colour map
                         # linewidth=1,         # wireframe line width
                         color='green')

        surf = ax.plot_wireframe(xx, yy, y_plot - y_sigma,      # data values (2D Arryas)
                 # rstride=2,           # row step size
                 # cstride=2,           # column step size
                 # cmap=cm.RdPu,        # colour map
                 # linewidth=1,         # wireframe line width
                 color='green')

        ax.set_xlabel('Methylation ($m$)')
        ax.set_ylabel('Ligand concentration ($L$)')
        ax.set_zlabel('Probability satisfaction for $\phi_{RUN}$')
        plt.show()
        # """

        return gp

    """
    # Changing Y_p over a time-step dt
    def dY_p(s, L=None, dt=0.05, Y_p=None):
        if Y_p is None:
            Y_p = s.Y_p
        # t = -0.01
        # while t < dt:
        # t += 0.01
        # dY_p = - (Y_p - Aa) / tau * dt
        # Y_p(t+dt) = -1/tau (Y_p - Y_p_mean) (no noise?)
        # dY_p = - (Y_p - 3) / tau * dt
        # dY_p = Y_p - s.Y_p  # + sp.random.normal(loc=0.0, scale=0.1)

        s.m += dm(L=L, dt=dt)
        s.m = max(0, s.m)
        aA = s.Y_p_mean(L)

        return aA - Y_p


    # Y_p_mean given L (chemoattractant concentration)
    def Y_p_mean(s, L):
        F = (eps_0 + eps_1 * s.m +
             n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
             n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
             )
        A = 1 / (1 + sp.exp(F))
        Y = alpha * A
        # Y = alpha / (1. + (sp.exp(eps_0 + eps_1 / L)
        # * ((1 + L * 100) / (1 + L * 10)) ** 7
        # )
        # )
        # print(s.m)
        # print(F)
        # print(A)

        # dmdt = 0.005 * (1 - 2 * A)
        return Y  # , dmdt

    # k+/- = om * exp(+/-(g_0/4 - g_1/2 * (Y_p/(Y_p + K_D))))
    def k_plus(s, Y_p):
        k = om * sp.exp(
            (g_0 / 4 - g_1 / 2 * (Y_p / (Y_p + K_D)))
        )
        return k

    def k_minus(s, Y_p):
        k = om * sp.exp(
            - (g_0 / 4 - g_1 / 2 * (Y_p / (Y_p + K_D)))
        )
        return k

    def f_r(s, L):
        f = (n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
             n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
             )
        return f

    def e_f_r(s, L):
        f = (((1 + L / K_TAR_off) / (1 + L / K_TAR_on)) ** n_TAR *
             ((1 + L / K_TSR_off) / (1 + L / K_TSR_on)) ** n_TSR
             )
        return f

    def smod_init(s):
        par = {
            'om': om,
            'g': g_0,
            'k_D': K_D,
            'eps_0': eps_0,
            'eps_1': eps_1,
            # 'k_TAR_off': K_TAR_off,
            # 'k_TAR_on': K_TAR_on,
            # 'k_TSR_off': K_TSR_off,
            # 'k_TSR_on': K_TSR_on,
            # 'n_TAR': n_TAR,
            # 'n_TSR': n_TSR,
            'alpha': alpha,
            'm_scale': scale
        }
        print(par)

        s.set_state_parameters(mod=s.smod, parameters=par)
    """
