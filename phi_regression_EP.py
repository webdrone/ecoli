import scipy as sp
# from scipy import linalg as la
# from scipy.stats import norm

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# using pyGPs
import pyGPs
import pickle


def GP_prob(extra_points, X_points, y, y_meta=None, y_pred_meta=None):
    # ARTIFICIAL POINTS
    """
    x = sp.linspace(min(X_points[:, 0]), max(X_points[:, 0]), 10)
    # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones((x.shape[0], 1))
    z_m = sp.zeros((x.shape[0], 1))
    print(y.shape, z_p.shape)
    X_points = sp.vstack((X_points,
                          sp.vstack((x, y_p)).T,
                          sp.vstack((x, y_m)).T))
    y = sp.vstack((y, z_p, z_m))
    """
    # Changing labels to {-1, 1} as required by pyGPs
    y[y == 0] = -1

    # Subsampling for hyperparameter optimization
    sample_size = min(10000, len(X_points))
    sample_idx = sp.random.choice(len(X_points),
                                  sample_size, replace=False)
    X_points_opt = X_points[sample_idx]
    y_opt = y[sample_idx]

    gp = pyGPs.GPC_FITC()
    gp.setData(x=X_points, y=y, value_per_axis=10)

    # m = pyGPs.mean.Zero()
    # m = pyGPs.mean.Linear(alpha_list=[-1, 10])
    # k = pyGPs.cov.RBFard(log_ell_list=[3.5, -2.5], log_sigma=3)
    k = pyGPs.cov.RBFard(log_ell_list=[3.5, -2.5], log_sigma=5)
    # m = pyGPs.mean.Zero()
    m = gp.meanfunc
    gp.setPrior(mean=m, kernel=k)

    # gp.optimize(numIterations=5)
    u = gp.u
    m = gp.meanfunc
    k_hyp = gp.covfunc.hyp
    print(gp.covfunc)
    print("Hyperparameter optimisation done.", k_hyp)

    """
    k = pyGPs.cov.RBFard(log_ell_list=k.hyp[0:1], log_sigma=k.hyp[-1])
    k.hyp = k_hyp

    gp = pyGPs.GPC_FITC()
    gp.setData(X_points, y)
    gp.setPrior(mean=m, kernel=k, inducing_points=u)
    """

    y_pred_mean, y_pred_sigma = gp.predict(extra_points)[:2]
    y_pred_sigma = (y_pred_sigma, y_pred_sigma)

    return y_pred_mean, y_pred_sigma, gp


def plot_GP(xx, yy, y_plot, y_plot_sigma, X, Y, y_trials):
    # Plotting scatter of observations, wireframe of GP mean
    fig = plt.figure()
    fig.tight_layout()
    # ax = fig.gca(projection='3d')               # 3d axes instance
    # surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arrays)
    #                          # rstride=2,           # row step size
    #                          # cstride=2,           # column step size
    #                          # cmap=cm.RdPu,        # colour map
    #                          # linewidth=1,         # wireframe line width
    #                          color='blue',
    #                          antialiased=True)

    # surf_sigma_plus = ax.plot_wireframe(xx, yy, y_plot_sigma[0],
    #                                     antialiased=True, color='green')

    # surf_sigma_minus = ax.plot_wireframe(xx, yy, y_plot_sigma[1],
    #                                      antialiased=True, color='green')
    # ax.scatter(X[:, 0], X[:, 1], Y / y_trials['trials'][:, 0] * 2 - 1,
    #            marker='x',
    #            c='r')

    # normalising [-1, 1] GP range to [0, 1]
    y_plot = y_plot / 2 + 0.5

    ax = fig.add_subplot(111)
    # im = ax.imshow(y_plot, interpolation='gaussian')
    p = ax.pcolormesh(xx, yy, y_plot,
                      shading='gouraud', cmap='plasma', vmin=0.0, vmax=1.0)

    X_pos_idx = Y >= 0.5
    X_neg_idx = sp.logical_not(X_pos_idx)
    print(sp.sum(X_pos_idx), sp.sum(X_neg_idx))

    # scatter plots such that less frequent classes are not obscured
    if sp.sum(X_pos_idx) < sp.sum(X_neg_idx):
        ax.scatter(X[X_neg_idx, 0], X[X_neg_idx, 1],
                   marker='x', c='cyan')
        ax.scatter(X[X_pos_idx, 0], X[X_pos_idx, 1],
                   marker='o', facecolors='none', edgecolors='k')
    else:
        ax.scatter(X[X_pos_idx, 0], X[X_pos_idx, 1],
                   marker='o', facecolors='none', edgecolors='k')
        ax.scatter(X[X_neg_idx, 0], X[X_neg_idx, 1],
                   marker='x', c='cyan')
    ax.set_xlabel('Methylation level ($m$) [dimensionless]')
    ax.set_ylabel('Ligand concentration ($L$) [mM]')
    # ax.set_zlabel('Probability satisfaction for $\phi_{RUN}$')
    # Add a colour bar along the bottom and label it
    cbar = fig.colorbar(ax=ax, mappable=p, orientation='vertical')
    cbar.set_label('Probability satisfaction for $\phi_{RUN}$')
    # plt.show()


def train_GP(run=True, plot=False):
    if run == 'both':
        m_L_prphi = sp.load('m_L_prphi_True.npy')
        m_L_prphi = sp.vstack((m_L_prphi, sp.load('m_L_prphi_False.npy')))
    else:
        m_L_prphi = sp.load('m_L_prphi_' + str(run) + '.npy')

    # Building sample
    sample_size = min(10000, len(m_L_prphi))
    sample_idx = sp.random.choice(len(m_L_prphi),
                                  sample_size, replace=False)
    m_L_prphi = m_L_prphi[sample_idx]

    N_trials = 1

    X = m_L_prphi[:, :2]
    Y = m_L_prphi[:, -1]
    if sp.any(Y < 0):
        print("Y<0")
        Y[Y < 0] = 0
    if sp.any(Y > 1):
        print("Y>1")
        Y[Y > 1] = 1

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    # points in the y axis
    y = sp.linspace(min(X[:, 1]), max(X[:, 1]), 100)
    xx, yy = sp.meshgrid(x, y)                # create the "base grid"
    x_pred = sp.column_stack((xx.ravel(), yy.ravel()))

    # GP for estimating phi_{RUN}
    y_gp = Y.reshape((len(Y), 1))

    # constructing metadata for observations (number of trials)
    y_trials = {'trials': (sp.ones(y_gp.shape) * N_trials).astype(int)}
    y_gp *= y_trials['trials']
    y_gp = sp.rint(y_gp)
    y_pred_trials = {'trials': (
        sp.ones((len(x_pred), 1)) * N_trials).astype(int)}

    print(y_gp[:10])
    print(y_gp.shape, y_trials['trials'].shape, y_pred_trials['trials'].shape)

    y_pred, y_pred_sigma, gp = GP_prob(x_pred, X, y_gp,
                                       y_meta=y_trials,
                                       y_pred_meta=y_pred_trials)
    # y_pred = y_pred[:, 0]
    y_plot = y_pred.reshape(sp.shape(xx))
    y_plot_sigma = (y_pred_sigma[0].reshape(sp.shape(xx)),
                    y_pred_sigma[1].reshape(sp.shape(xx)))

    # Saving trained GP
    gpEP_phi_run_pkl = gp
    pickle.dump(gpEP_phi_run_pkl, open('gpEP_phi_run_FITC_' +
                                       str(run) +
                                       '.pkl', 'wb'))
    if plot:
        plot_GP(xx, yy, y_plot, y_plot_sigma, X, Y, y_trials)
        plt.title("Run: {}".format(run))


if __name__ == '__main__':
    train_GP(run=True, plot=True)
    train_GP(run=False, plot=True)
    # train_GP(run='both', plot=True)
    plt.show()
