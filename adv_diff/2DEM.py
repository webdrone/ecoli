import scipy as sp
from scipy import linalg as la
from scipy import stats
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

num_sims = 30
num_dims = 2

y_init = sp.ones(num_dims) * 2
t_init = 0
t_end = 2
dt = 0.001

N = int((t_end - t_init) / dt)

# c_theta = 0.7
# c_mu = 1.5
# c_sigma = 0.06

c_D = sp.eye(num_dims)
# c_V_D = sp.ones(num_dims)
v0 = 1
KA = 10 ** 3
KI = 10 ** -3

# Ligand field
L0 = 10 ** 0
L_mean = 0 * sp.ones(num_dims)
S = 1 * sp.eye(num_dims)


def L(x):
    L_x = (L0 * (2 * sp.pi) * sp.sqrt(la.det(S)) *
           stats.multivariate_normal.pdf(x, mean=L_mean, cov=S))
    return L_x


def grad_L(x):
    grad_L_x = la.inv(S) @ (L_mean - x) * L(x)
    return grad_L_x


def mu(y, t):
    # m = c_theta * (c_mu - y)
    # m = c_V_D
    L_y = L(y)
    m = grad_L(y) * v0 * (KA - KI) / ((KA + L_y) * (KI + L_y))
    return m


def sigma(y, t):
    return sp.sqrt(c_D)


def dW(dt):
    # W = sp.random.normal(loc=0.0, scale=sp.sqrt(dt))
    W = sp.random.multivariate_normal(mean=sp.zeros(dt.shape), cov=sp.diag(dt))
    return W


def plot_field(y):
    nx = 100
    ny = 100

    x = sp.linspace(min(y[:, :, 0].flatten()), max(y[:, :, 0].flatten()), nx)
    y = sp.linspace(min(y[:, :, 1].flatten()), max(y[:, :, 1].flatten()), ny)

    xx, yy = sp.meshgrid(x, y)
    h = L(sp.vstack((xx.flatten(), yy.flatten())).T).reshape(nx, ny)

    fig = plt.figure()
    s = fig.add_subplot(1, 1, 1, xlabel='$x$', ylabel='$y$')
    im = s.imshow(
        h,
        extent=(x[0],
                x[-1],
                y[0],
                y[-1]),
        norm=LogNorm(vmin=h.min(), vmax=h.max()),  # L(sp.zeros(2)),
        origin='lower')
    fig.colorbar(im)


t = sp.arange(t_init, t_end, dt)
y = sp.zeros((num_sims, N, num_dims))
y[:, 0] = y_init
dt = sp.ones(num_dims) * dt

for i_sim in range(num_sims):
    y_isim = y[i_sim]
    for i in range(1, t.size):
        a = mu(y_isim[i - 1], (i - 1) * dt)
        b = sigma(y_isim[i - 1], (i - 1) * dt)
        y_isim[i] = y_isim[i - 1] + a * dt + b @ dW(dt)
y = sp.array(y)

# Plotting trajectories
plot_field(y)
for y_i in y:
    plt.plot(y_i[:, 0], y_i[:, 1])

# CMC
y_dist = sp.sqrt(sp.sum(y ** 2, axis=-1))
y_dist /= y_dist[0, 0]
CMC = sp.sum(y_dist, axis=0) / num_sims
CMC_var = sp.sum(y_dist ** 2, axis=0) / num_sims - CMC ** 2
CMC_std = sp.sqrt(CMC_var)

fig = plt.figure()
plt.plot(t, CMC)
plt.plot(t, CMC + CMC_std, c='green')
plt.plot(t, CMC - CMC_std, c='green')
plt.show()
