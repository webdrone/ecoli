import scipy as sp
from scipy import stats
from scipy import linalg as la
import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation


dx = 0.01
dy = 0.01
a = 1
timesteps = 50
t = 0.

x_max = 3
y_max = 3

nx = int(x_max / dx)
ny = int(y_max / dy)


dx2 = dx**2
dy2 = dy**2

dt = dx2 * dy2 / (2 * a * (dx2 + dy2))

ui = sp.zeros([nx, ny])
u = sp.zeros([nx, ny])

for i in range(nx):
    for j in range(ny):
        if (((i * dx - 2.5)**2 + (j * dy - 2.5)**2 <= 0.1)
                & ((i * dx - 2.5)**2 + (j * dy - 2.5)**2 >= .05)):
            ui[i, j] = 1
ui[1:5, 1:15] = 1

v0 = 10
KA = 1
KI = 0.01

# Ligand field
C = sp.zeros((int(nx + 2), int(ny + 2)))
S = 3 * sp.eye(2)
for i in range(-1, nx + 1):
    for j in range(-1, ny + 1):
        x = sp.array([i * dx, j * dy])
        C[i, j] = (10 * (2 * sp.pi) * sp.sqrt(la.det(S)) *
                   stats.multivariate_normal.pdf(x, mean=sp.zeros(2), cov=S))

grad_C = sp.stack((
    (C[2:, 1:-1] - C[:-2, 1:-1]) / (2 * dx),
    (C[1:-1, 2:] - C[1:-1, :-2]) / (2 * dy)),
    axis=-1)

# v = sp.ones([nx, ny, 2]) * 100
# v[:, :, 0] = 0


def evolve_ts(u, ui):
    # grad_u = sp.stack((
    #     (ui[2:, 1:-1] - ui[:-2, 1:-1]) / (2 * dx),
    #     (ui[1:-1, 2:] - ui[1:-1, :-2]) / (2 * dy)),
    #     axis=-1)

    # adv = sp.sum(v[1:-1, 1:-1] * grad_u, axis=-1)
    C_mul = sp.stack((C[1:-1, 1:-1], C[1:-1, 1:-1]), axis=-1)
    ui_mul = sp.stack((ui, ui), axis=-1)
    v = grad_C * v0 * (KA - KI) / ((KA + C_mul) * (KI + C_mul))
    vb = v * ui_mul
    grad_vb = ((vb[2:, 1:-1, 0] - vb[:-2, 1:-1, 0]) / (2 * dx) +
               (vb[1:-1, 2:, 1] - vb[1:-1, :-2, 1]) / (2 * dy))

    u[1:-1, 1:-1] = ui[1:-1, 1:-1] + a * dt * (
        (ui[2:, 1:-1] - 2 * ui[1:-1, 1:-1] + ui[:-2, 1:-1]) / dx2 +
        (ui[1:-1, 2:] - 2 * ui[1:-1, 1:-1] + ui[1:-1, :-2]) / dy2) \
        - dt * 10 * (grad_vb)


def data_gen(framenumber, Z, surf):
    global u
    global ui
    evolve_ts(u, ui)
    ui[:] = u[:]
    Z = ui

    ax.clear()
    plotset()
    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False, alpha=0.7)
    return surf,


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

X = sp.arange(0, x_max, dx)
Y = sp.arange(0, y_max, dy)
X, Y = sp.meshgrid(X, Y)

Z = ui


def plotset():
    ax.set_xlim3d(0., x_max)
    ax.set_ylim3d(0., y_max)
    ax.set_zlim3d(-1., 1.)
    ax.set_autoscalez_on(False)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    cset = ax.contour(X, Y, Z, zdir='x', offset=0., cmap=cm.coolwarm)
    cset = ax.contour(X, Y, Z, zdir='y', offset=1., cmap=cm.coolwarm)
    cset = ax.contour(X, Y, Z, zdir='z', offset=-1., cmap=cm.coolwarm)

plotset()
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False, alpha=0.7)


fig.colorbar(surf, shrink=0.5, aspect=5)

ani = animation.FuncAnimation(fig, data_gen, fargs=(
    Z, surf), frames=timesteps, interval=30, blit=False)
ani.save("2dDiffusion.mp4", bitrate=1024)

print("done")
# plt.show()
