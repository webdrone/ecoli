import scipy as sp
import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation


dx = 0.2
dy = 0.2
a = 0.5
timesteps = 50
t = 0.

nx = int(1 / dx)
ny = int(1 / dy)


dx2 = dx**2
dy2 = dy**2

dt = dx2 * dy2 / (2 * a * (dx2 + dy2))

ui = sp.zeros([nx, ny])
u = sp.zeros([nx, ny])

for i in range(nx):
    for j in range(ny):
        if (((i * dx - 0.5)**2 + (j * dy - 0.5)**2 <= 0.1) &
            ((i * dx - 0.5)**2 + (j * dy - 0.5)**2 >= .05)):
            ui[i, j] = 1

v = sp.ones([nx, ny, 2])
v[:, :, 1] = 0

def evolve_ts(u, ui):
    grad_u = sp.stack((
        (ui[2:, 1:-1] - ui[:-2, 1:-1]) / (2 * dx),
        (ui[1:-1, 2:] - ui[1:-1, :-2]) / (2 * dy)),
        axis=-1)

    u[1:-1, 1:-1] = ui[1:-1, 1:-1] + a * dt * (
        (ui[2:, 1:-1] - 2 * ui[1:-1, 1:-1] + ui[:-2, 1:-1]) / dx2 +
        (ui[1:-1, 2:] - 2 * ui[1:-1, 1:-1] + ui[1:-1, :-2]) / dy2)
    return grad_u


print(ui)
gr = evolve_ts(u, ui)
print(gr)
print(gr.shape, v.shape)

print(sp.sum(v[1:-1, 1:-1] * gr, axis=-1))
