import os
import io
from contextlib import redirect_stdout
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import stochpy
from config import *

# HELPER FUNCTIONS


def set_state_parameters(mod=None, state=None, parameters=None):
    if state is not None:
        # Changing model state
        for k, v in state.items():
            mod.ChangeInitialSpeciesCopyNumber(k, int(v))
    if parameters is not None:
        # Changing parameters
        # (for k_+ and k_- which need to be updated
        # according to concentration Y_p at every step.)
        for k, v in parameters.items():
            mod.ChangeParameter(k, v)


def m_mean(L):
    g = g_0
    Y_ss = K_D * ((1 - 2 / g * sp.log(1 / mb_CW - 1)) /
                  (1 + 2 / g * sp.log(1 / mb_CW - 1))
                  )

    # Using Emonet's reply.
    m = -1 / eps_1 * (eps_0 +
                      n_TAR * (sp.log(1 + L / K_TAR_off) -
                               sp.log(1 + L / K_TAR_on)) +
                      n_TSR * (sp.log(1 + L / K_TSR_off) -
                               sp.log(1 + L / K_TSR_on)) -
                      sp.log(alpha / Y_ss - 1)
                      )
    # print(-1 / eps_1 * (eps_0 +
    #                     n_TAR * (sp.log(1 + L / K_TAR_off) -
    #                              sp.log(1 + L / K_TAR_on)) +
    #                     n_TSR * (sp.log(1 + L / K_TSR_off) -
    #                              sp.log(1 + L / K_TSR_on))))
    # print(1 / eps_1 * sp.log(alpha / Y_ss - 1))
    # print(alpha, Y_ss, alpha/Y_ss -1)
    if m < 0:
        print("Negative methylation.")
        m = max(0, m)
    return m


def f_r(L):
    f = (n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
         n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
         )
    return f

# Defining 'RUN' property


def phi(trajectory, x_min=2):
    s_phi = 0
    # for s in trajectory:
    s = trajectory[-1]
    if sp.any(s[1]) or sp.any(s[3] < x_min):
        return s_phi
    s_phi = 1
    return s_phi


def estimate_phi(run=True):
    ##############
    # SETTING UP #
    ##############

    # Specifying SSA model (Flagellum/motor CTMC)
    smodel = 'No_meth.psc'
    smodel_dir = os.path.dirname(os.path.realpath(__file__))
    smodel = os.path.join(smodel_dir, smodel)

    # Starting stochpy SSA object (Flagellum/motor)
    smod = stochpy.SSA(IsInteractive=False, IsQuiet=True)
    smod.Model(smodel)

    # State and parameters
    state_local = dict(state)
    del state_local['M']
    n_flag = sum([v for k, v in state_local.items()])
    print("Flagellar number:", n_flag)
    if run:
        state_local = {'S': 0,
                       'C': n_flag - 2,
                       'N': 2}
    else:
        state_local = {'S': 1,
                       'C': n_flag - 1,
                       'N': 0}

    parameters = {
        'mu': 5.0,
        'om': om,
        'g': g_0,
        'k_D': K_D,
        'eps_0': eps_0,
        'eps_1': eps_1,
        # 'k_TAR_off': K_TAR_off,
        # 'k_TAR_on': K_TAR_on,
        # 'k_TSR_off': K_TSR_off,
        # 'k_TSR_on': K_TSR_on,
        # 'n_TAR': n_TAR,
        # 'n_TSR': n_TSR,
        'alpha': alpha,
        # 'm_scale': scale,
        'm': 0  # s.m_mean(L) * scale
    }

    ##############
    # SIMULATION #
    ##############
    m_L_prphi = []
    m_min, m_max, dm = (0.0, 100, 1)
    L_min, L_max, dL = (0.0, 0.2, 0.005)

    # Squelching output
    with io.StringIO() as buf, redirect_stdout(buf):
        # if True:
        m = m_min
        while m_min <= m <= m_max:
            # parameters['m'] = m
            L = L_min
            while L_min <= L <= L_max:
                # Setting state and parameters
                pars = {'f_r': f_r(L),
                        # 'e_f_r': s.e_f_r(L),
                        # 'lambda': 1 / tau,
                        # 'beta': m_mean(L) * scale / tau
                        'm': m
                        }
                # merging parameter dictionaries to set new parameters
                parameters = {**parameters, **pars}
                # print(parameters['m'], L, parameters['f_r'])
                # state_local['M'] = m * parameters['m_scale']
                if run:
                    random_flag_num = sp.random.randint(2, n_flag + 1)
                    state_local = {'S': 0,
                                   'C': n_flag - random_flag_num,
                                   'N': random_flag_num}
                else:
                    random_flag_num = sp.random.randint(0, n_flag + 1)
                    if random_flag_num == 0:
                        random_flag_num2 = sp.random.randint(n_flag - 1, n_flag + 1)
                    else:
                        random_flag_num2 = sp.random.randint(0, n_flag - random_flag_num + 1)

                    state_local = {'S': random_flag_num,
                                   'C': random_flag_num2,
                                   'N': n_flag - (random_flag_num +
                                                  random_flag_num2)}

                set_state_parameters(mod=smod, state=state_local,
                                     parameters=parameters)

                # Simulating
                N_traj = 100

                smod.DoStochSim(method="direct",
                                trajectories=N_traj,
                                mode="time",
                                end=dt)
                # Estimating p(phi)
                p_phi = 0.0
                for t in range(N_traj):
                    smod.GetTrajectoryData(t + 1)
                    species = smod.data_stochsim.getSpecies()
                    p_phi += phi(species)
                p_phi /= N_traj
                m_L_prphi += [[m, L, p_phi]]
                print(m_L_prphi[-1])
                L += dL
            m += dm

    m_L_prphi = sp.array(m_L_prphi)
    print(m_L_prphi)

    # Saving (m, L, pr(phi)) data
    savename = 'm_L_prphi_' + str(run)
    sp.save(savename, m_L_prphi)

    # Plotting in scatter
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('$Meth$')
    ax.set_ylabel('$L$')
    ax.set_zlabel('$\phi_{RUN}$')
    ax.scatter(s_state_phi[:, 0], s_state_phi[:, 1], s_state_phi[:, 2])
    plt.show()
    """


if __name__ == '__main__':
    estimate_phi(run=True)
    estimate_phi(run=False)
