import scipy as sp
import os
from scipy import stats
from scipy import linalg as la
from config import *
from sklearn.externals import joblib
from scipy.stats import norm


class Tumbling_angle_dist(stats.rv_continuous):

    def _pdf(self, x):
        return (0.5 * (1 + sp.cos(x)) * sp.sin(x))


class Ecoli:
    # USER SETTINGS
    # (MODEL TO BE USED, PARAMETERS)

    # Specifying SSA model (Flagellum/motor CTMC)
    smodel = 'gp_phi_run'
    smodel_dir = os.path.dirname(os.path.realpath(__file__))
    smodel = os.path.join(smodel_dir, smodel)

    # Specifying SSA model (Methylation CTMC)
    # smodel_meth = "Methylation.psc"
    # smodel_meth = os.path.join(smodel_dir, smodel_meth)

    tumbling_angle_dist = Tumbling_angle_dist()

    # Weiner variable for methylation OU process
    def dW(dt):
        return sp.random.normal(loc=0.0, scale=sp.sqrt(dt))

    def __init__(s, L=0.0,
                 pos=sp.ones(2) * 5, vel=sp.array([0.5, 0])):
        # loading trained GP for phi_RUN
        s.gp_True = joblib.load(Ecoli.smodel + '_True.pkl')
        s.gp_False = joblib.load(Ecoli.smodel + '_False.pkl')

        # If no distinction made
        # s.gp_True = joblib.load(Ecoli.smodel + '_both.pkl')
        # s.gp_False = s.gp_True

        # s.std_scaler_True = joblib.load('std_scaler' + '_True.pkl')
        # s.std_scaler_False = joblib.load('std_scaler' + '_False.pkl')

        # Methylation
        # s.smod_meth = stochpy.SSA(IsInteractive=False, IsQuiet=True)
        # s.smod_meth.Model(Ecoli.smodel_meth)

        s.m = s.m_mean(L)
        s.L = L  # Doesn't really matter

        # Setting time, location, and velocity
        s.t = 0.0
        # keeping track of running, tumbling times
        s.t_run = []
        s.t_tumble = [0.0]
        s.run_flag = 0
        s.pos = sp.array(pos)
        s.vel = sp.array(vel)
        s.tumble()
        s.trajectory = sp.asmatrix(sp.hstack((0.0, s.pos)))

    def simulate(s, L=None, n_ecoli=1, dt=0.05):
        s.L = L

        extra_points = sp.array([[s.m, s.L]])

        proba = True

        # priors after boundary y = e^x
        # if (L > (sp.exp(s.m / 35 * 3) - 0.9) / (3 * 35)):
        #     p_pred = [[1, 1]]
        # elif (L < (sp.exp((s.m - 20) / 35 * 3) - 0.9) / (3 * 35)):
        #     p_pred = [[0, 0]]
        # print("predicting..", extra_points, la.norm(s.pos))

        if s.run_flag:
            # extra_points = s.std_scaler_True.transform(extra_points)
            if proba:
                p_pred = s.gp_True.predict_proba(extra_points)
            else:
                p_pred = s.gp_True.predict(extra_points)
        else:
            # extra_points = s.std_scaler_False.transform(extra_points)
            if proba:
                p_pred = s.gp_False.predict_proba(extra_points)
            else:
                p_pred = s.gp_False.predict(extra_points)

        # """
        # Transforming back into [0, 1] domain, probit, \Phi(y) and norm
        # p_pred = norm.cdf(p_pred)
        # Normalising
        # p_pred = p_pred / (sp.sum(p_pred, axis=1)[:, None])
        # p_pred = p_pred[:, 0]
        # """
        # print("Done")

        if proba:
            p_pred = p_pred[0, 1]

        s.move(dt=dt, p_run=p_pred)

        # Evolve m
        s.m += s.dm(L=L, dt=dt)
        s.m = max(0, s.m)

        # Updating Y_p intra-cellular concentration
        # s.Y_p += s.dY_p(L=L, dt=dt)
        # print(s.Y_p)

    def move(s, dt=0.05, p_run=None):
        s.t += dt

        if sp.random.uniform() > p_run:
            s.tumble(dt=dt)
            traj = sp.hstack((s.t, s.pos))
            s.trajectory = sp.vstack((s.trajectory, traj))
            # print(s.t)
            # Collecting tumbling times
            if s.run_flag:
                s.t_tumble += [dt]
                s.run_flag = 0
            else:
                s.t_tumble[-1] += dt
        else:
            s.run(dt=dt)
            # Collecting run times
            if s.run_flag:
                s.t_run[-1] += dt
            else:
                s.t_run += [dt]
                s.run_flag = 1

    def run(s, dt=0.05):
        s.pos += s.vel * dt

    def tumble(s, dt=0.05):
        # Assuming theta_dist gives absolute values
        sign = sp.random.choice(sp.array([-1, 1]))
        theta = theta_dist()
        theta = sign * theta

        # omega = sp.pi
        # theta = sign * omega * dt

        # dt = theta / omega
        # t = 0.0
        # while t < dt:
        #     s.Y_p += s.dY_p(L, dt=0.01)
        #     t += 0.01
        # print(theta)
        cos_t = sp.cos(theta)
        sin_t = sp.sin(theta)
        R = sp.array([[cos_t, -sin_t], [sin_t, cos_t]])
        s.vel = R @ s.vel

    # HELPER FUNCTIONS
    def m_mean(s, L):
        g = g_0
        Y_ss = K_D * ((1 - 2 / g * sp.log(1 / mb_CW - 1)) /
                      (1 + 2 / g * sp.log(1 / mb_CW - 1))
                      )

        # Using Emonet's reply.
        m = -1 / eps_1 * (eps_0 +
                          n_TAR * (sp.log(1 + L / K_TAR_off) -
                                   sp.log(1 + L / K_TAR_on)) +
                          n_TSR * (sp.log(1 + L / K_TSR_off) -
                                   sp.log(1 + L / K_TSR_on)) -
                          sp.log(alpha / Y_ss - 1)
                          )
        # print(-1 / eps_1 * (eps_0 +
        #                     n_TAR * (sp.log(1 + L / K_TAR_off) -
        #                              sp.log(1 + L / K_TAR_on)) +
        #                     n_TSR * (sp.log(1 + L / K_TSR_off) -
        #                              sp.log(1 + L / K_TSR_on))))
        # print(1 / eps_1 * sp.log(alpha / Y_ss - 1))
        # print(alpha, Y_ss, alpha/Y_ss -1)
        if m < 0:
            print("Negative methylation.")
        m = max(0, m)
        return m

    # Changing Y_p over a time-step dt
    def dY_p(s, L=None, dt=0.05, Y_p=None):
        if Y_p is None:
            Y_p = s.Y_p
        # t = -0.01
        # while t < dt:
        # t += 0.01
        # dY_p = - (Y_p - Aa) / tau * dt
        # Y_p(t+dt) = -1/tau (Y_p - Y_p_mean) (no noise?)
        # dY_p = - (Y_p - 3) / tau * dt
        # dY_p = Y_p - s.Y_p  # + sp.random.normal(loc=0.0, scale=0.1)

        s.m += dm(L=L, dt=dt)
        s.m = max(0, s.m)
        aA = s.Y_p_mean(L)

        return aA - Y_p

    def dm(s, L, dt=0.05):
        # Wiener process for methylation updates
        sigma_m = sp.sqrt(tau * C) / alpha
        dm = dt / tau * (s.m_mean(L) - s.m) \
            + sigma_m * sp.sqrt(2 / tau) * Ecoli.dW(dt)
        return dm

    """
    # Y_p_mean given L (chemoattractant concentration)
    def Y_p_mean(s, L):
        F = (eps_0 + eps_1 * s.m +
             n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
             n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
             )
        A = 1 / (1 + sp.exp(F))
        Y = alpha * A
        # Y = alpha / (1. + (sp.exp(eps_0 + eps_1 / L)
        # * ((1 + L * 100) / (1 + L * 10)) ** 7
        # )
        # )
        # print(s.m)
        # print(F)
        # print(A)

        # dmdt = 0.005 * (1 - 2 * A)
        return Y  # , dmdt

    # k+/- = om * exp(+/-(g_0/4 - g_1/2 * (Y_p/(Y_p + K_D))))
    def k_plus(s, Y_p):
        k = om * sp.exp(
            (g_0 / 4 - g_1 / 2 * (Y_p / (Y_p + K_D)))
        )
        return k

    def k_minus(s, Y_p):
        k = om * sp.exp(
            - (g_0 / 4 - g_1 / 2 * (Y_p / (Y_p + K_D)))
        )
        return k

    def f_r(s, L):
        f = (n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
             n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
             )
        return f

    def e_f_r(s, L):
        f = (((1 + L / K_TAR_off) / (1 + L / K_TAR_on)) ** n_TAR *
             ((1 + L / K_TSR_off) / (1 + L / K_TSR_on)) ** n_TSR
             )
        return f

    def smod_init(s):
        par = {
            'om': om,
            'g': g_0,
            'k_D': K_D,
            'eps_0': eps_0,
            'eps_1': eps_1,
            # 'k_TAR_off': K_TAR_off,
            # 'k_TAR_on': K_TAR_on,
            # 'k_TSR_off': K_TSR_off,
            # 'k_TSR_on': K_TSR_on,
            # 'n_TAR': n_TAR,
            # 'n_TSR': n_TSR,
            'alpha': alpha,
            'm_scale': scale
        }
        print(par)

        s.set_state_parameters(mod=s.smod, parameters=par)
    """
