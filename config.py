import scipy as sp
from scipy import stats
import scipy.linalg as la

outname = 'CTMC.dat'
# ====================
# Model parameters
# ====================

# -----------
# Active model
# -----------

# -----------
# Emonet (elife) MWC values
# -----------

tau = 5  # Adaptation rate (sec)
mb_CW = 0.15  # Tumble bias
alpha = 7  # This is \alpha * Y_Tot (micro M)

# MWC
eps_0 = 1  # Interpolated, and * (n_TAR + n_TSR)
eps_1 = -0.5  # Interpolated, and * (n_TAR + n_TSR)
K_TAR_off = 0.002  # mM
K_TAR_on = 0.012  # mM
K_TSR_off = 0.1  # mM
K_TSR_on = 10**3  # mM
n_TAR = 6
n_TSR = 12
C = 3.89 * 10 ** -3
scale = 3 * 10 ** 3  # Scale for pCTMC (which substitutes OU process)

# Flagellar/motor
om = 1.3  # sec^-1
g_0 = 40  # k_B T
g_1 = g_0  # k_B T
K_D = 3.06  # mM

# Movement
vel = sp.array([0.02, 0])
# 20mm reported, but everywhere else it is 20 micro-m so going with the latter.


def theta_dist():
    t = sp.random.gamma(shape=4, scale=18.32) / 180 * sp.pi
    return t


# ====================
# Simulation conditions
# ====================
n_ecoli = 100
pos = sp.ones(2) * 3  # Initial E. coli position
t_end = 500.0  # Simulation running time
dt = 0.05  # Time step for SDE integration
state = {
    "S": 5,
    "C": 0,
    "N": 0,
    "M": 60000
}  # Initial state for flagellae


# Chemecial concentration L field (2D gaussian pdf in this case)
def L_field(x, t=0):
    # For neutral random walk, always same L(0,0)
    # x = sp.zeros(sp.shape(x))

    # L1 field
    S = 3 * sp.eye(2)
    L = (0.1 * (2 * sp.pi) * sp.sqrt(la.det(S)) *
         stats.multivariate_normal.pdf(x, mean=sp.zeros(2), cov=S))

    # # L2 field
    # A = sp.array([[1 / 5, 0], [0, 1 / 2]])
    # L = sp.maximum(10 ** (-5), 0.1 - (0.05 * sp.sqrt(x.T @ A.T @ A @ x)))

    # # L3 field
    # S = 3 * (t / 50 + 1) * sp.eye(2)
    # L = (0.1 * (2 * sp.pi) * sp.sqrt(la.det(S)) *
    #      stats.multivariate_normal.pdf(x, mean=sp.zeros(2), cov=S))
    return L


# ====================
# Model parameters -- Models in papers
# ====================
# -----------
# Sneddon et al.
# -----------
'''
# Phenomenological values
tau = 15
mb_CW = 0.25
alpha = 6

# MWC
eps_0 = 1
eps_1 = -0.45
K_TAR_off = 0.02  # mM
K_TAR_on = 0.4  # mM
K_TSR_off = 100  # mM
K_TSR_on = 10**6  # mM
n_TAR = 6
n_TSR = 13

# Movement
vel = sp.array([0.02, 0])  # mm


def theta_dist():
    t = sp.random.gamma(shape=4, scale=18.32) / 180 * sp.pi
    return t

# Flagellar/motor
om = 1.3  # sec^-1
g_0 = 40  # k_B T
g_1 = g_0  # k_B T
K_D = 3.06  # mM
'''

# -----------
# Emonet (elife) MWC values
# -----------
'''
tau = 10  # Adaptation rate (sec)
mb_CW = 0.15  # Tumble bias
# alpha = ??  # This is \alpha * Y_Tot (micro M)

# Phenomenological values
# tau = # varies [1, 20] (sec)
# mb_CW = # varies [0, 0.6]
# alpha = # varies [] (micro M)

# MWC
eps_0 = 6
eps_1 = -1
K_TAR_off = 0.0182  # mM
K_TAR_on = 3  # mM
K_TSR_off = 10  # mM
K_TSR_on = 10**6  # mM
n_TAR = 2
n_TSR = 4

# Flagellar/motor
om = 1.3  # sec^-1
g_0 = 40  # k_B T
g_1 = g_0  # k_B T
K_D = 3.06  # mM

# Movement
vel = sp.array([0.02, 0])
# 20mm reported, but everywhere else it is 20 micro-m so going with the latter.


def theta_dist():
    t = sp.random.gamma(shape=4, scale=18.32) / 180 * sp.pi
    return t
'''

# -----------
# Vladimirov
# -----------
'''
# Phenomenological values
# tau = # N/A  # sec
# mb_CW = 0.35
# alpha = # a Y_tot: a = ? Y_tot = 9.7  # micro M

# MWC
eps_0 = 1 * 18  # Interpolated, and * (n_TAR + n_TSR)
eps_1 = -0.5 * 18  # Interpolated, and * (n_TAR + n_TSR)
K_TAR_off = 0.002  # mM
K_TAR_on = 0.012  # mM
K_TSR_off = 0.1  # mM
K_TSR_on = 10**3  # mM
n_TAR = 6
n_TSR = 12

# Flagellar/motor
om = 1.3  # sec^-1
g_0 = 40  # k_B T
g_1 = g_0  # k_B T
K_D = 3.06  # mM

# Movement
vel = sp.array([2, 0])  # mm


def theta_dist():
    t = sp.arccos(2 * sp.sqrt(1 - sp.rand()) - 1)
    return t
'''
