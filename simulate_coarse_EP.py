import scipy as sp
from scipy.linalg import norm
import Ecoli_coarse_EP
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import config


def L(x, t):
    L = config.L_field(x, t)
    return L


def ecoli_sim(n=1, plot=True):
    # INITIAL CONDITIONS for E. coli
    t = int(0 * 100)
    t_end = int(config.t_end * 100)
    dt = int(config.dt * 100)

    # Calculating error of original position
    err = sp.linalg.norm(config.pos)
    print("Starting average error", err)
    err = 0
    ave_traj = sp.zeros((int(t_end / dt) + 1, 7))
    ave_traj[0] = sp.array(sp.hstack((t / 100, config.pos, config.pos ** 2,
                                      norm(config.pos), norm(config.pos) ** 2)))

    # Loop iterations = number of E. coli simulations
    for i in range(n):
        print("Ecoli", i + 1, "of", n)
        t = int(0 * 100)
        ecoli = Ecoli_coarse_EP.Ecoli(L=L(config.pos, t / 100),
                                      pos=config.pos, vel=config.vel)
        print(ecoli.pos, L(ecoli.pos, t / 100), ecoli.m)

        # print("L at", ecoli.pos, "=", L(ecoli.pos, t / 100),
        #       "(E. coli position)")
        # print("L at", sp.zeros(2), "=", L(sp.zeros, t / 100(2)))
        # print("E. coli Y_p: ", ecoli.Y_p)

        while t < t_end:
            # print(L(ecoli.pos, t / 100))
            # print(L(ecoli.pos, t / 100), ecoli.Y_p)

            ecoli.simulate(dt=config.dt, L=L(ecoli.pos, t / 100))
            t += dt
            # print(t)
            if t % dt == 0:
                # print("Time elapsed:", t, "seconds.")
                ave_traj[int(t / dt), 0] = t / 100
                ave_traj[int(t / dt), 1:3] += ecoli.pos / n
                ave_traj[int(t / dt), 3:5] += (ecoli.pos ** 2) / n
                ave_traj[int(t / dt), 5] += norm(ecoli.pos) / n
                ave_traj[int(t / dt), 6] += norm(ecoli.pos) ** 2 / n

        traj = sp.hstack((ecoli.t, ecoli.pos))
        ecoli.trajectory = sp.vstack((ecoli.trajectory, traj))

        # Dumping full E. coli trajectory and run/tumble times
        sp.save('coarseEP_traj' + str(i + 1), ecoli.trajectory)
        sp.save('coarseEP_run' + str(i + 1), ecoli.t_run)
        sp.save('coarseEP_tumble' + str(i + 1), ecoli.t_tumble)

        # Plotting
        t_run = []
        t_tumble = []
        if plot:
            # plot_ecoli(ecoli)

            # RUN/TUMBLE times histogram
            t_run += ecoli.t_run
            t_tumble += ecoli.t_tumble

        # Adding final E. coli position to error count
        err += sp.linalg.norm(ecoli.pos)
    # Calculating variance for ave_traj: Var[X] = E[X^2] - (E[X])^2
    ave_traj[:, 3:5] = ave_traj[:, 3:5] - ave_traj[:, 1:3] ** 2
    ave_traj[:, 6] = ave_traj[:, 6] - ave_traj[:, 5] ** 2

    if plot:
        # RUN/TUMBLE times histogram
        plt.figure()
        plt.hist(sp.array(t_run))
        plt.title("Run times histogram")
        plt.savefig("coarseEP_" + str(n) + "ecoli_hist_run.png")
        plt.figure()
        plt.hist(sp.array(t_tumble))
        plt.title("Tumble times histogram")
        plt.savefig("coarseEP_" + str(n) + "ecoli_hist_tumble.png")

    return err / n, ave_traj


def plot_ecoli(ecoli):
    # Plotting
    # """
    x_min = sp.hstack((ecoli.trajectory[:, 1],
                       ecoli.trajectory[:, -1])).min() - 1
    x_max = sp.hstack((ecoli.trajectory[:, 1],
                       ecoli.trajectory[:, -1])).max() + 1

    nx = 100  # int((x_max - x_min)/0.1)
    ny = nx
    y_min = x_min
    y_max = x_max

    x = sp.linspace(x_min, x_max, nx)
    y = sp.linspace(y_min, y_max, ny)

    xx, yy = sp.meshgrid(x, y)
    h = L(sp.vstack((xx.flatten(), yy.flatten())).T).reshape(nx, ny)

    fig = plt.figure()
    s = fig.add_subplot(1, 1, 1, xlabel='$x$', ylabel='$y$')
    im = s.imshow(
        h,
        extent=(x[0],
                x[-1],
                y[0],
                y[-1]),
        norm=LogNorm(vmin=h.min(), vmax=h.max()),  # L(sp.zeros(2)),
        origin='lower')
    fig.colorbar(im)

    cs = plt.cm.gray(ecoli.trajectory[:, 0])[0, 0]
    s.plot(ecoli.trajectory[0, 1], ecoli.trajectory[0, -1],
           c='red', marker='*', ms=10)
    s.plot(ecoli.trajectory[:, 1], ecoli.trajectory[:, -1],
           color=cs, marker='^', ms=3)
    s.plot(ecoli.trajectory[-1, 1], ecoli.trajectory[-1, -1],
           c='white', marker='^', ms=10)
    # """

    print("vel", ecoli.vel)
    print()
    print("Running statistics (mean, std dev):", len(ecoli.t_run))
    print(sp.mean(ecoli.t_run), sp.std(ecoli.t_run))
    print("Tumbling statistics (mean, std dev):", len(ecoli.t_tumble))
    print(sp.mean(ecoli.t_tumble), sp.std(ecoli.t_tumble))
    # plt.show()


err_tot, ave_traj = ecoli_sim(int(config.n_ecoli), plot=False)
print(err_tot)
print(ave_traj)
sp.save('ave_ecoli_coarse_EP', ave_traj)
plt.show()
