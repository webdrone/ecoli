import scipy as sp
import os
import stochpy
from scipy import stats
from config import *


class Tumbling_angle_dist(stats.rv_continuous):

    def _pdf(self, x):
        return (0.5 * (1 + sp.cos(x)) * sp.sin(x))


class Ecoli:
    # USER SETTINGS
    # (MODEL TO BE USED, PARAMETERS)

    # Specifying SSA model (Flagellum/motor CTMC)
    smodel = "Methylation.psc"
    smodel_dir = os.path.dirname(os.path.realpath(__file__))
    smodel = os.path.join(smodel_dir, smodel)

    # Specifying SSA model (Methylation CTMC)
    # smodel_meth = "Methylation.psc"
    # smodel_meth = os.path.join(smodel_dir, smodel_meth)

    tumbling_angle_dist = Tumbling_angle_dist()

    # Weiner variable for methylation OU process
    def dW(dt):
        return sp.random.normal(loc=0.0, scale=sp.sqrt(dt))

    def __init__(s, state=None, parameters=None, L=0.0,
                 pos=sp.ones(2) * 5, vel=sp.array([0.5, 0]), outfile=None):
        # Starting stochpy SSA object (Flagellum/motor)
        s.smod = stochpy.SSA(IsInteractive=False, IsQuiet=True)
        s.smod.Model(Ecoli.smodel)

        s.outfile = outfile

        # Methylation
        # s.smod_meth = stochpy.SSA(IsInteractive=False, IsQuiet=True)
        # s.smod_meth.Model(Ecoli.smodel_meth)

        s.m = s.m_mean(L)
        s.Y_p = 3  # Doesn't really matter

        if state is None:
            s.state = {
                "S": 4,
                "C": 0,
                "N": 0,
                "M": s.m_mean(L) * scale
            }
        else:
            s.state = dict(state)

        if parameters is None:
            s.parameters = {
                "mu": 5.0
                # "k_plus": s.k_plus(s.Y_p),
                # "k_minus": s.k_minus(s.Y_p)
            }
        else:
            s.parameters = dict(parameters)

        # Setting state and parameters
        s.set_state_parameters(mod=s.smod, state=state, parameters=parameters)
        s.smod_init()

        # Setting time, location, and velocity
        s.t = 0.0
        # keeping track of running, tumbling times
        s.t_run = []
        s.t_tumble = [0.0]
        s.run_flag = 0
        s.pos = sp.array(pos)
        s.vel = sp.array(vel)
        s.tumble()
        s.trajectory = sp.asmatrix(sp.hstack((0.0, s.pos)))

    def set_state_parameters(s, mod=None, state=None, parameters=None):

        if state is not None:
            # Changing model state
            for k, v in state.items():
                mod.ChangeInitialSpeciesCopyNumber(k, int(v))
                s.state[k] = v
        if parameters is not None:
            # Changing parameters
            # (for k_+ and k_- which need to be updated
            # according to concentration Y_p at every step.)
            for k, v in parameters.items():
                mod.ChangeParameter(k, v)
                s.parameters[k] = v

    def simulate(s, state=None, parameters=None, L=None,
                 n_ecoli=1, dt=0.05):
        # Calculating k+ and k- according to current Y_p
        # s.Y_p += s.dY_p(L=L, dt=dt)
        # print(s.Y_p, s.parameters['k_minus'])
        # s.smod_init()
        if parameters is None:
            parameters = {
                # 'l': L,
                'f_r': s.f_r(L),
                # 'e_f_r': s.e_f_r(L),
                'lambda': 1 / tau,
                'beta': s.m_mean(L) * scale / tau
            }
        # print(parameters)
        s.set_state_parameters(mod=s.smod, parameters=parameters)
        # print(L, s.m, s.Y_p)

        # Simulating
        N_traj = n_ecoli
        t_end = dt
        s.smod.DoStochSim(method="direct",
                          trajectories=N_traj,
                          mode="time",
                          end=t_end)

        # Changing model state to reflect last state simulated
        species_labels = s.smod.data_stochsim.species_labels
        species_state = s.smod.data_stochsim.species[-1]
        state_new = dict(zip(species_labels, species_state))

        s.set_state_parameters(mod=s.smod, state=state_new)
        s.move(dt=dt)

        # Writing CTMC to file
        if s.outfile is not None:
            species = s.smod.data_stochsim.getSpecies()
            species_save = sp.hstack((species, L * sp.ones((len(species), 1))))
            sp.savetxt(s.outfile, species_save)

        # Updating Y_p intra-cellular concentration
        # s.Y_p += s.dY_p(L=L, dt=dt)
        # print(s.Y_p)

    def move(s, dt=0.05, x_min=2):
        species = s.smod.data_stochsim.getSpecies()

        # print(species)
        # for spec in range(1, len(species)):
        #     delta_t = species[spec, 0] - species[spec - 1, 0]
        #     s.t += delta_t
        s.t += dt
        # (sp.any(species[-1, 1]) or sp.any(species[-1, 3] < x_min))
        # sp.any(species[-1, 3] < 3)
        if (sp.any(species[-1, 1]) or sp.any(species[-1, 3] < x_min)):
            s.tumble(dt=dt)
            traj = sp.hstack((s.t, s.pos))
            s.trajectory = sp.vstack((s.trajectory, traj))
            # print(s.t)
            # Collecting tumbling times
            if s.run_flag:
                s.t_tumble += [dt]
                s.run_flag = 0
            else:
                s.t_tumble[-1] += dt
        else:
            s.run(dt=dt)
            # Collecting run times
            if s.run_flag:
                s.t_run[-1] += dt
            else:
                s.t_run += [dt]
                s.run_flag = 1

    def run(s, dt=0.05):
        s.pos += s.vel * dt

    def tumble(s, dt=0.05):
        # Assuming theta_dist gives absolute values
        sign = sp.random.choice(sp.array([-1, 1]))
        theta = theta_dist()
        theta = sign * theta

        # omega = sp.pi
        # theta = sign * omega * dt

        # dt = theta / omega
        # t = 0.0
        # while t < dt:
        #     s.Y_p += s.dY_p(L, dt=0.01)
        #     t += 0.01
        # print(theta)
        cos_t = sp.cos(theta)
        sin_t = sp.sin(theta)
        R = sp.array([[cos_t, -sin_t], [sin_t, cos_t]])
        s.vel = R @ s.vel

    # HELPER FUNCTIONS
    def m_mean(s, L):
        g = g_0
        Y_ss = K_D * ((1 - 2 / g * sp.log(1 / mb_CW - 1)) /
                      (1 + 2 / g * sp.log(1 / mb_CW - 1))
                      )

        # Using Emonet's reply.
        m = -1 / eps_1 * (eps_0 +
                          n_TAR * (sp.log(1 + L / K_TAR_off) -
                                   sp.log(1 + L / K_TAR_on)) +
                          n_TSR * (sp.log(1 + L / K_TSR_off) -
                                   sp.log(1 + L / K_TSR_on)) -
                          sp.log(alpha / Y_ss - 1)
                          )
        # print(-1 / eps_1 * (eps_0 +
        #                     n_TAR * (sp.log(1 + L / K_TAR_off) -
        #                              sp.log(1 + L / K_TAR_on)) +
        #                     n_TSR * (sp.log(1 + L / K_TSR_off) -
        #                              sp.log(1 + L / K_TSR_on))))
        # print(1 / eps_1 * sp.log(alpha / Y_ss - 1))
        # print(alpha, Y_ss, alpha/Y_ss -1)
        if m < 0:
            print("Negative methylation.")
        m = max(0, m)
        return m

    """
    # Y_p_mean given L (chemoattractant concentration)
    def Y_p_mean(s, L):
        F = (eps_0 + eps_1 * s.m +
             n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
             n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
             )
        A = 1 / (1 + sp.exp(F))
        Y = alpha * A
        # Y = alpha / (1. + (sp.exp(eps_0 + eps_1 / L)
        # * ((1 + L * 100) / (1 + L * 10)) ** 7
        # )
        # )
        # print(s.m)
        # print(F)
        # print(A)

        # dmdt = 0.005 * (1 - 2 * A)
        return Y  # , dmdt

    # Changing Y_p over a time-step dt
    def dY_p(s, L=None, dt=0.05, Y_p=None):
        if Y_p is None:
            Y_p = s.Y_p
        # t = -0.01
        # while t < dt:
        # t += 0.01
        # dY_p = - (Y_p - Aa) / tau * dt
        # Y_p(t+dt) = -1/tau (Y_p - Y_p_mean) (no noise?)
        # dY_p = - (Y_p - 3) / tau * dt
        # dY_p = Y_p - s.Y_p  # + sp.random.normal(loc=0.0, scale=0.1)

        # Wiener process for methylation updates
        '''
        sigma_m = sp.sqrt(tau * C) / alpha
        dm = dt / tau * (s.m_mean(L) - s.m) \
            + sigma_m * sp.sqrt(2 / tau) * Ecoli.dW(dt)
        s.m += dm
        '''
        # Methylation CTMC
        # scale is number of methylation particles in pCTMC
        M_bar = s.m_mean(L) * scale

        # Setting birth (\beta), death (\lambda) rates to match OU
        s.smod_meth.ChangeParameter('beta', M_bar / tau)
        s.smod_meth.ChangeParameter('lambda', 1 / tau)

        # Setting initial condition to be the current meth level
        s.smod_meth.ChangeInitialSpeciesCopyNumber('M', s.m * scale)

        s.smod_meth.DoStochSim(method="direct",
                               trajectories=1,
                               mode="time",
                               end=dt)

        m_traj = s.smod_meth.data_stochsim.getSpecies()
        s.m = m_traj[-1, 1] / scale
        # s.m = min(max(0, s.m), 4)

        aA = s.Y_p_mean(L)

        return aA - Y_p

    # k+/- = om * exp(+/-(g_0/4 - g_1/2 * (Y_p/(Y_p + K_D))))
    def k_plus(s, Y_p):
        k = om * sp.exp(
            (g_0 / 4 - g_1 / 2 * (Y_p / (Y_p + K_D)))
        )
        return k

    def k_minus(s, Y_p):
        k = om * sp.exp(
            - (g_0 / 4 - g_1 / 2 * (Y_p / (Y_p + K_D)))
        )
        return k
    """

    def f_r(s, L):
        f = (n_TAR * (sp.log(1 + L / K_TAR_off) - sp.log(1 + L / K_TAR_on)) +
             n_TSR * (sp.log(1 + L / K_TSR_off) - sp.log(1 + L / K_TSR_on))
             )
        return f

    def e_f_r(s, L):
        f = (((1 + L / K_TAR_off) / (1 + L / K_TAR_on)) ** n_TAR *
             ((1 + L / K_TSR_off) / (1 + L / K_TSR_on)) ** n_TSR
             )
        return f

    def smod_init(s):
        par = {
            'om': om,
            'g': g_0,
            'k_D': K_D,
            'eps_0': eps_0,
            'eps_1': eps_1,
            # 'k_TAR_off': K_TAR_off,
            # 'k_TAR_on': K_TAR_on,
            # 'k_TSR_off': K_TSR_off,
            # 'k_TSR_on': K_TSR_on,
            # 'n_TAR': n_TAR,
            # 'n_TSR': n_TSR,
            'alpha': alpha,
            'm_scale': scale
        }
        print(par)

        s.set_state_parameters(mod=s.smod, parameters=par)
