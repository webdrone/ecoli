import scipy as sp
import os
import stochpy
import matplotlib.pyplot as plt

# Specifying SSA model
smodel = "Methylation.psc"
smodel_dir = os.path.dirname(os.path.realpath(__file__))
smodel = os.path.join(smodel_dir, smodel)
smod = stochpy.SSA(IsInteractive=False, IsQuiet=True)
smod.Model(smodel)

# Simulating
N_traj = 1
t_end = 50

M_bar = 3000
tau = 15

state = {'M': 1000}
parameters = {'beta': M_bar / tau,
              'lambda': 1 / tau 

              }

for k, v in state.items():
    smod.ChangeInitialSpeciesCopyNumber(k, int(v))
for k, v in parameters.items():
    smod.ChangeParameter(k, v)

smod.DoStochSim(method="direct",
                trajectories=N_traj,
                mode="time",
                end=t_end)

smod.PlotSpeciesTimeSeries()


# Comparing with OU process
scale = 3 * 10 ** 3
m = state['M'] / scale
m_bar = M_bar / scale
dt = 0.05
t = 0.0
traj = sp.array([t, m])


# Weiner variable for methylation OU process
def dW(dt):
    return sp.random.normal(loc=0.0, scale=sp.sqrt(dt))


# Wiener process for methylation updates
sigma_m = sp.sqrt(tau * 3.89 * 10 ** -3) / 7
while t < t_end:
    dm = dt / tau * (m_bar - m) + sigma_m * sp.sqrt(2 / tau) * dW(dt)
    m += dm
    t += dt
    traj = sp.vstack((traj, sp.array([t, m])))
plt.figure()
plt.plot(traj[:, 0], traj[:, -1] * scale, c='red')
plt.show()
