# Stochastic Simulation Algorithm input file
# Methylation CTMC 
# OU process, dm/dt = 1/tau (m_bar - m(t)) + dW, Wiener process
# Birth/Death CTMC, birth rate = m_bar/tau, death rate = 1/tau

# Flag-motor in E. coli, no delay
# CW_S (--> CW_C) --> CCW_N --> CW_S

# Flag/Motor Reactions
Flag1:
    S > C
    S * mu

Motor1a:
    C > N
    C * (om * eul ** (g/2 * (1/2 - (1 / (1 + k_D / (alpha / ( 1 + eul ** (eps_0 + eps_1 * m + f_r))))))))
    # C * k_plus
  	# *mu/(mu-k_plus)


Motor1b:
    S > N
    S * (om * eul ** (g/2 * (1/2 - (1 / (1 + k_D / (alpha / ( 1 + eul ** (eps_0 + eps_1 * m + f_r))))))))
    # S * k_plus

Motor2:
    N > S
    N * (om * eul ** (-g/2 * (1/2 - (1 / (1 + k_D / (alpha / ( 1 +  eul ** (eps_0 + eps_1 * m + f_r))))))))
    # N * k_minus

"""
# Methylation Reactions
Birth:
    $pool > M
    beta
    # beta

Death:
    M > $pool
    lambda * M
    # lambda * M
"""

# Variable species

# Flag/motor
S = 10
C = 0
N = 0

# Methylation
# M = 3000
m = 1.0

# Parameters
"""
# Methylation
beta = 1
lambda = 0.5
"""

# Flag/motor

mu = 5.0
f_r = 0.0

alpha = 6
eps_0 = 1
eps_1 = -0.5
# m_scale = 3000


om = 1.3
g = 40
k_D = 3.06

eul = 2.71828

"""
k_TAR_off = 0.002
k_TAR_on = 0.012
k_TSR_off = 0.1
k_TSR_on = 10 ** 3
n_TAR = 6
n_TSR = 12
l = 0.1
"""

"""
!F y = alpha / ( 1 + eul ** (eps_0 + eps_1 * M/m_scale + f_r))

!F y_p = alpha / ( 1 + eul ** (eps_0 + eps_1 * M/m_scale) * ((1 + l / k_TAR_off)/(1 + l / k_TAR_on)) ** n_TAR * ((1 + l / k_TSR_off) /(1 + l / k_TSR_on)) ** n_TSR)

!F k_plus = om * exp(g/2 * (1/2 - (y/(y+k_D))))
!F k_minus = om * exp(-g/2 * (1/2 - (y/(y+k_D))))

!F k_plus = om * eul ** (g/2 * (1/2 - (1 / (1 + k_D/(alpha / ( 1 + eul ** (eps_0 + eps_1 * M/m_scale) * ((1 + l / k_TAR_off)/(1 + l / k_TAR_on)) ** n_TAR * ((1 + l / k_TSR_off) /(1 + l / k_TSR_on)) ** n_TSR))))))
!F k_minus = om * eul ** (-g/2 * (1/2 - (1/ (1 + k_D/(alpha / ( 1 + eul ** (eps_0 + eps_1 * M/m_scale) * ((1 + l / k_TAR_off)/(1 + l / k_TAR_on)) ** n_TAR * ((1 + l / k_TSR_off) /(1 + l / k_TSR_on)) ** n_TSR))))))
"""
