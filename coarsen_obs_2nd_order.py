import os
import io
from contextlib import redirect_stdout
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import stochpy
from config import *


import Ecoli_nometh as Ecoli
import config


# HELPER FUNCTIONS

def L(x):
    L = config.L_field(x)
    return L


def ecoli_sim(n=1, m_iter=1):
    m_L_prphi_TT = []
    m_L_prphi_TF = []
    m_L_prphi_FT = []
    m_L_prphi_FF = []

    # INITIAL CONDITIONS for E. coli
    t = 0 * 100
    t_end = int(config.t_end * 100)
    dt = int(config.dt * 100)
    pos = config.pos
    vel = config.vel
    state = config.state

    L_x = L(pos)

    # Loop iterations = number of E. coli simulations
    for i in range(n):
        print("E. coli", i + 1, "of", n)

        t = int(0 * 100)
        ecoli = Ecoli.Ecoli(state=state, parameters=None, L=L_x,
                            pos=pos, vel=vel, outfile=None)

        # print("L at", ecoli.pos, "=", L(ecoli.pos),
        #       "(E. coli position)")
        # print("L at", sp.zeros(2), "=", L(sp.zeros(2)))
        # print("E. coli Y_p: ", ecoli.Y_p)

        # Adding some noise to starting methylation and ligand concentration
        # to provide support for the
        # observations over the domain of m, L, away from the steady state.
        # print("Starting methylation level:", ecoli.m)
        # ecoli.m += sp.randn() * ecoli.m
        # ecoli.m = max(0, ecoli.m)
        # print("Starting methylation level after noise:", ecoli.m)

        print("Starting ligand:", L(ecoli.pos))
        ecoli.pos += ecoli.pos * sp.randn() / la.norm(ecoli.pos)
        ecoli.L = L(ecoli.pos)
        print("Starting ligand after noise:", L(ecoli.pos))
        run_flag2 = ecoli.run_flag
        while t < t_end:
            # print(L(ecoli.pos))
            # print(L(ecoli.pos), ecoli.Y_p)

            # Observations conditioned on previous state of cell.
            if ecoli.run_flag:
                if run_flag2:
                    m_L_prphi = m_L_prphi_TT
                else:
                    m_L_prphi = m_L_prphi_TF
            else:
                if run_flag2:
                    m_L_prphi = m_L_prphi_FT
                else:
                    m_L_prphi = m_L_prphi_FF

            run_flag2 = ecoli.run_flag

            if t % (10 * 100) == 0:
                print("randomising", t / 100)
                # ecoli.m += sp.randn() * ecoli.m
                # ecoli.m = max(0, ecoli.m)

                ecoli.pos += ecoli.pos * sp.randn() / la.norm(ecoli.pos)
                ecoli.L = L(ecoli.pos)

            m_L_prphi += [[ecoli.m, L(ecoli.pos), 0]]
            m_i_pos = sp.array(ecoli.pos)
            m_i_state = dict(ecoli.state)
            m_i_meth = ecoli.m
            for m_i in range(m_iter):
                ecoli.pos = m_i_pos
                ecoli.m = m_i_meth
                # Squelching output
                with io.StringIO() as buf, redirect_stdout(buf):
                    ecoli.simulate(dt=config.dt, L=L(ecoli.pos),
                                   state=m_i_state)

                m_L_prphi[-1][-1] += int(ecoli.run_flag) / m_iter
            t += dt

    return sp.array([m_L_prphi_TT, m_L_prphi_TF, m_L_prphi_FT, m_L_prphi_FF])


def estimate_phi():
    ##############
    # SIMULATION #
    ##############
    m_L_prphis = ecoli_sim(n=5, m_iter=1)

    m_L_prphi_TT = m_L_prphis[0]
    m_L_prphi_TF = m_L_prphis[1]
    m_L_prphi_FT = m_L_prphis[2]
    m_L_prphi_FF = m_L_prphis[3]

    # print(m_L_prphi_True)
    # print(m_L_prphi_False)

    # Saving (m, L, pr(phi)) data
    sp.save('m_L_prphi_TT', m_L_prphi_TT)
    sp.save('m_L_prphi_TF', m_L_prphi_TF)
    sp.save('m_L_prphi_FT', m_L_prphi_FT)
    sp.save('m_L_prphi_FF', m_L_prphi_FF)


if __name__ == '__main__':
    estimate_phi()
