import os
import scipy as sp
from scipy import linalg as la
from scipy.stats import norm

# from sklearn.gaussian_process import GaussianProcessClassifier
# from sklearn.gaussian_process import GaussianProcessRegressor
# from sklearn.gaussian_process import GaussianProcess
# from sklearn.gaussian_process.kernels import RBF
# from sklearn.gaussian_process.kernels import Matern
# from sklearn.neighbors import KNeighborsClassifier
# from sklearn.preprocessing import StandardScaler
# from sklearn.neural_network import MLPClassifier
# from sklearn.ensemble import AdaBoostClassifier
# from sklearn.ensemble import RandomForestClassifier
# using pyGPs
import pyGPs
import pickle

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def GP_prob(extra_points, X_points, y):
    # ARTIFICIAL POINTS
    """
    x = sp.linspace(min(X_points[:, 0]), max(X_points[:, 0]), 10)
    # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones((x.shape[0], 1))
    z_m = sp.zeros((x.shape[0], 1))
    print(y.shape, z_p.shape)
    X_points = sp.vstack((X_points,
                          sp.vstack((x, y_p)).T,
                          sp.vstack((x, y_m)).T))
    y = sp.vstack((y, z_p, z_m))
    """

    # Changing labels to {-1, 1} as required by pyGPs
    y[y == 0] = -1

    # Subsampling for hyperparameter optimization
    # sample_size = min(500, len(X_points))
    # sample_idx = sp.random.choice(len(X_points),
    #                               sample_size, replace=False)
    # X_points_opt = X_points[sample_idx]
    # y_opt = y[sample_idx]

    gp = pyGPs.GPC_FITC()
    gp.setData(x=X_points, y=y, value_per_axis=8)

    # m = pyGPs.mean.Zero()
    k = pyGPs.cov.RBFard(log_ell_list=[1, 0.01], log_sigma=1.)
    # m = pyGPs.mean.Zero()
    gp.setPrior(kernel=k)

    gp.optimize()
    u = gp.u
    m = gp.meanfunc
    k_hyp = gp.covfunc.hyp
    k = pyGPs.cov.RBFard(log_ell_list=[1, 0.01], log_sigma=1.)
    k.hyp = k_hyp
    print(gp.covfunc)
    print("Hyperparameter optimisation done.", k_hyp)

    # gp = pyGPs.GPC_FITC()
    # gp.setData(X_points, y)
    # gp.setPrior(mean=m, kernel=k, inducing_points=u)
    # gp.getPosterior()

    y_pred_mean, y_pred_sigma = gp.predict(
        extra_points, ys=sp.ones((extra_points.shape[0], 1)))[:2]
    y_pred_sigma = (y_pred_sigma, y_pred_sigma)

    return y_pred_mean, gp


def plot_GP(xx, yy, y_plot, X, Y):
    fig = plt.figure()
    ax = fig.gca(projection='3d')               # 3d axes instance
    surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arryas)
                             # rstride=2,           # row step size
                             # cstride=2,           # column step size
                             # cmap=cm.RdPu,        # colour map
                             # linewidth=1,         # wireframe line width
                             antialiased=True)
    ax.scatter(X[:, 0], X[:, 1], Y,
               marker='x',
               c='r')
    ax.set_xlabel('Methylation ($m$)')
    ax.set_ylabel('Ligand concentration ($L$)')
    ax.set_zlabel('Probability satisfaction for $\phi_{RUN}$')

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones(x.shape)
    z_m = sp.zeros(x.shape)
    ax.plot(x, y_p, z_p, color='r')
    ax.plot(x, y_m, z_m, color='r')


def train_GP(run=True, run2=True, plot=False):
    if run == 'both':
        m_L_prphi = sp.load('m_L_prphi_True.npy')
        m_L_prphi = sp.vstack((m_L_prphi, sp.load('m_L_prphi_False.npy')))
    else:
        if run:
            if run2:
                run_name = 'TT'
            else:
                run_name = 'TF'
        else:
            if run2:
                run_name = 'FT'
            else:
                run_name = 'FF'

        m_L_prphi = sp.load('m_L_prphi_' + run_name + '.npy')

    # m_L_prphi_Run = m_L_prphi[m_L_prphi[:, -1] >= 0.5]
    # m_L_prphi_Tumble = m_L_prphi[m_L_prphi[:, -1] < 0.5]
    sample_size = min(2000, len(m_L_prphi))

    # sample_size = min(3000, len(m_L_prphi_Run), len(m_L_prphi_Tumble))

    print("sample size:", sample_size)
    # Building sample without losing edge points
    # sample_idx_run = sp.random.choice(len(m_L_prphi_Run), len(
    #     m_L_prphi_Run) - sample_size, replace=False)
    # sample_idx_tumble = sp.random.choice(len(m_L_prphi_Tumble), len(
    #     m_L_prphi_Tumble) - sample_size, replace=False)

    # m_L_prphi = sp.vstack((
    #     sp.delete(m_L_prphi_Run, sample_idx_run, axis=0),
    #     sp.delete(m_L_prphi_Tumble, sample_idx_tumble, axis=0)))

    sample_idx = sp.random.choice(len(m_L_prphi), sample_size, replace=False)
    m_L_prphi = m_L_prphi[sample_idx]

    X = m_L_prphi[:, :2]
    Y = m_L_prphi[:, -1]
    if sp.any(Y < 0):
        print("Y<0")
        Y[Y < 0] = 0
    Y[Y > 1] = 1

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    # points in the y axis
    y = sp.linspace(min(X[:, 1]), max(X[:, 1]), 100)
    xx, yy = sp.meshgrid(x, y)                # create the "base grid"
    x_pred = sp.column_stack((xx.ravel(), yy.ravel()))
    print(x_pred.shape)

    # GP for estimating phi_{RUN}
    y_gp = Y.reshape((len(Y), 1))
    # y_gp = sp.hstack((y_gp, sp.ones(sp.shape(y_gp)) - y_gp))
    y_pred, gp = GP_prob(x_pred, X, y_gp)
    # y_pred = y_pred[:, 0]
    y_plot = y_pred.reshape(sp.shape(xx))

    # Saving trained GP
    gpEP_phi_run_pkl = gp
    pickle.dump(gpEP_phi_run_pkl, open('gpEP_phi_run_FITC_' +
                                       str(run_name) +
                                       '.pkl', 'wb'))

    if plot:
        plot_GP(xx, yy, y_plot, X, Y)


if __name__ == '__main__':
    train_GP(run=True, run2=True, plot=True)
    train_GP(run=True, run2=False, plot=True)
    train_GP(run=False, run2=True, plot=True)
    train_GP(run=False, run2=False, plot=True)
    # train_GP(run='both', plot=True)
    plt.show()
