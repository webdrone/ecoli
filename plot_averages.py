import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

ave_traj = sp.load('ave_ecoli.npy')
ave_traj_coarse_EP = sp.load('ave_ecoli_coarse_EP.npy')


def plot_traj(ave_traj, title="Mean distance to max"):
    plot_ave_traj = sp.hstack((
        ave_traj[:, 0].reshape((len(ave_traj), 1)),
        sp.sqrt(ave_traj[:, 1]**2 + ave_traj[:, 2]**2).reshape((len(ave_traj), 1)),
        sp.sqrt(ave_traj[:, 3] + ave_traj[:, 4]).reshape((len(ave_traj), 1))
    ))
    print(plot_ave_traj.shape)
    print(plot_ave_traj[:5])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(plot_ave_traj[:, 0], plot_ave_traj[:, 1], c='b')
    ax.plot(plot_ave_traj[:, 0],
            plot_ave_traj[:, 1] + plot_ave_traj[:, 2], c='r')
    ax.plot(plot_ave_traj[:, 0],
            plot_ave_traj[:, 1] - plot_ave_traj[:, 2], c='r')
    # ax.plot(ave_traj[:, 0], ave_traj[:, 5], c='b')
    # ax.plot(ave_traj[:, 0],
    #         ave_traj[:, 5] + sp.sqrt(ave_traj[:, 6]), c='r')
    # ax.plot(ave_traj[:, 0],
    #         ave_traj[:, 5] - sp.sqrt(ave_traj[:, 6]), c='r')
    ax.set_title(title)
    ax.set_ylabel("Mean distance from origin (mm)")
    ax.set_xlabel("Time (s)")
    ax.set_ylim((3.5, 4.5))


plot_traj(ave_traj, title="Ecoli micro, OU methylation")
plot_traj(ave_traj_coarse_EP, title="Ecoli coarse EP")

plt.show()
