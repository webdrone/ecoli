import os
import scipy as sp
from scipy import linalg as la
from scipy.stats import norm

from sklearn.externals import joblib
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegressionCV

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def GP_prob(extra_points, X_points, y):
    # Normalising m, L dimensions
    std_scaler = StandardScaler()
    std_scaler.fit(X_points)
    X_points = std_scaler.transform(X_points)
    extra_points = std_scaler.transform(extra_points)

    # Constructing RBF kernels, distributing them in m, L space
    mus_x = sp.linspace(min(X_points[:, 0]), max(X_points[:, 0]), 10)
    mus_y = sp.linspace(min(X_points[:, 1]), max(X_points[:, 1]), 10)
    mus = sp.vstack((mus_x, mus_y)).T
    g = 1 / (len(X_points) ** (1 / 16))
    X_rbf = rbf_kernel(X_points, Y=mus, gamma=g)
    extra_rbf = rbf_kernel(extra_points, Y=mus, gamma=g)

    logreg = LogisticRegressionCV()
    logreg.fit(X_rbf, y)
    print(logreg.get_params())

    y_pred = logreg.predict_proba(extra_rbf)
    y_pred = y_pred[:, 1]

    return y_pred, logreg, std_scaler, mus, g


def plot_GP(xx, yy, y_plot, X, Y):
    fig = plt.figure()
    ax = fig.gca(projection='3d')               # 3d axes instance
    surf = ax.plot_wireframe(xx, yy, y_plot,      # data values (2D Arryas)
                             # rstride=2,           # row step size
                             # cstride=2,           # column step size
                             # cmap=cm.RdPu,        # colour map
                             # linewidth=1,         # wireframe line width
                             antialiased=True)
    ax.scatter(X[:, 0], X[:, 1], Y,
               marker='x',
               c='r')
    ax.set_xlabel('Methylation ($m$)')
    ax.set_ylabel('Ligand concentration ($L$)')
    ax.set_zlabel('Probability satisfaction for $\phi_{RUN}$')

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    y_p = (sp.exp(x / 35 * 3) - 0.9) / (3 * 35)
    y_m = (sp.exp((x - 20) / 35 * 3) - 0.9) / (3 * 35)
    z_p = sp.ones(x.shape)
    z_m = sp.zeros(x.shape)
    ax.plot(x, y_p, z_p, color='r')
    ax.plot(x, y_m, z_m, color='r')
    plt.show()


def train_GP(run=True, plot=False):
    if run == 'both':
        m_L_prphi = sp.load('m_L_prphi_True.npy')
        m_L_prphi = sp.vstack((m_L_prphi, sp.load('m_L_prphi_False.npy')))
    else:
        m_L_prphi = sp.load('m_L_prphi_' + str(run) + '.npy')

    m_L_prphi_Run = m_L_prphi[m_L_prphi[:, -1] >= 0.5]
    m_L_prphi_Tumble = m_L_prphi[m_L_prphi[:, -1] < 0.5]
    sample_size = min(4000, len(m_L_prphi_Run), len(m_L_prphi_Tumble))

    print("sample size:", sample_size)
    # Building sample without losing edge points
    sample_idx_run = sp.random.choice(len(m_L_prphi_Run), len(
        m_L_prphi_Run) - sample_size, replace=False)
    sample_idx_tumble = sp.random.choice(len(m_L_prphi_Tumble), len(
        m_L_prphi_Tumble) - sample_size, replace=False)

    m_L_prphi = sp.vstack((
        sp.delete(m_L_prphi_Run, sample_idx_run, axis=0),
        sp.delete(m_L_prphi_Tumble, sample_idx_tumble, axis=0)))

    X = m_L_prphi[:, :2]
    Y = m_L_prphi[:, -1]
    if sp.any(Y < 0):
        print("Y<0")
        Y[Y < 0] = 0
    Y[Y > 1] = 1

    x = sp.linspace(min(X[:, 0]), max(X[:, 0]), 100)  # points in the x axis
    # points in the y axis
    y = sp.linspace(min(X[:, 1]), max(X[:, 1]), 100)
    xx, yy = sp.meshgrid(x, y)                # create the "base grid"
    x_pred = sp.column_stack((xx.ravel(), yy.ravel()))
    print(x_pred.shape)

    # GP for estimating phi_{RUN}
    y_gp = Y.reshape((len(Y), 1))
    # y_gp = sp.hstack((y_gp, sp.ones(sp.shape(y_gp)) - y_gp))
    y_pred, gp, std_scaler, mus, gamma = GP_prob(x_pred, X, y_gp)
    # y_pred = y_pred[:, 0]
    y_plot = y_pred.reshape(sp.shape(xx))

    # Saving trained GP
    joblib.dump(gp, 'rbf_phi_run_' + str(run) + '.pkl')
    if std_scaler is not None:
        print("Scaler on.")
        joblib.dump(std_scaler, 'std_scaler_' + str(run) + '.pkl')
    sp.save('rbf_mus_gamma', sp.vstack((mus, gamma * sp.ones(mus.shape[-1]))))
    if plot:
        plot_GP(xx, yy, y_plot, X, Y)


if __name__ == '__main__':
    train_GP(run=True, plot=True)
    train_GP(run=False, plot=True)
    # train_GP(run='both', plot=True)
